//
//  UITextFieldExtension.swift
//  UCash
//
//  Created by Sagar.Gupta on 05/06/17.
//  Copyright © 2016 Hiteshi Infotech. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {

    /// Sets placehoder text color
    ///
    /// - Parameter placeHoderTextColor: placeHoderTextColor color to set for the placehoder text.
   internal func setPlaceHolderTextColor(placeHoderTextColor: UIColor) {
        let mutable = NSMutableAttributedString(string: placeholder!)
        let range = NSRange.init(location: 0, length: placeholder!.count)
    mutable.addAttribute(kCTForegroundColorAttributeName as NSAttributedString.Key, value: placeHoderTextColor, range: range)
        attributedPlaceholder = mutable
    }

    /// Adds a done button on textfield to hide the keyboard. Useful when showing number pad.
    internal func addDoneButton() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(resignFirstResponder))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        inputAccessoryView = keyboardToolbar
    }

    internal func addTextFieldtintcolor() {
        tintColor = UIColor.white
    }
    
    internal func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    internal func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    internal func setRightPaddingWithImage(_ amount:CGFloat,_ image:UIImage) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        let imageVW = UIImageView(image: image)
        imageVW.frame = CGRect(x: -3, y: 15, width: 15, height: 10)
        imageVW.contentMode = .scaleAspectFit
        paddingView.addSubview(imageVW)
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
   internal func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor(red: 182/255.0, green: 182/255.0, blue: 182/255.0, alpha: 0.8).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
}
