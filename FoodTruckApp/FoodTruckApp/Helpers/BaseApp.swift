//
//  BaseApp.swift
//  FoodTruckApp
//
//  Created by MacBook Pro on 02/08/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class BaseApp: NSObject {
    
    static let sharedInstance = BaseApp()
    
    //MARK:- Show common alert view controller
    func showAlertViewControllerWith(title:String?, message:String, buttonTitle:String?, controller:UIViewController?){
        let banner = NotificationBanner(title: title!, subtitle: message, style: .success)
        banner.backgroundColor = UIColor.red
        banner.titleLabel?.textColor = UIColor.white
        banner.subtitleLabel?.textColor = UIColor.white
        banner.show()
    }
}
