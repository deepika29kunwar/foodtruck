//
//  ViewLayerSetup.swift
//  Ikan
//

import Foundation
import UIKit

class ViewLayerSetup: UIView{
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0{
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0{
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        didSet {
            layer.shadowColor = shadowColor?.cgColor
        }
    }
    
    @IBInspectable var masksToBounds: Bool = false{
        didSet {
            layer.masksToBounds = masksToBounds
        }
    }
    
    @IBInspectable var shadowOffset: CGSize = CGSize.zero{
        didSet {
            layer.shadowOffset = shadowOffset
        }
    }
}

class GradientView: UIView {
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors = [ #colorLiteral(red: 0.2823529412, green: 0.3215686275, blue: 0.937254902, alpha: 1).cgColor , #colorLiteral(red: 0.431372549, green: 0.5333333333, blue: 0.9725490196, alpha: 1).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5);
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5);
    }
}

class ButtonLayerSetup: UIButton{
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0{
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0{
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        didSet {
            layer.shadowColor = shadowColor?.cgColor
        }
    }
    
    @IBInspectable var masksToBounds: Bool = false{
        didSet {
            layer.masksToBounds = masksToBounds
        }
    }
    
    @IBInspectable var shadowOffset: CGSize = CGSize.zero{
        didSet {
            layer.shadowOffset = shadowOffset
        }
    }
    
    @IBInspectable var multiLineText: Bool = false{
        didSet {
            if(multiLineText){
                titleLabel?.numberOfLines = 0
            } else {
                titleLabel?.numberOfLines = 1
            }
        }
    }
    
    @IBInspectable var alignmentCenter: Bool = false{
        didSet {
            if(alignmentCenter){
                titleLabel?.textAlignment = .center
            } else {
                titleLabel?.textAlignment = .left
            }
        }
    }
}


class ButtonConfiguration: UIButton{
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let radius: CGFloat = self.bounds.size.height*0.50
        self.layer.cornerRadius = radius
        layer.masksToBounds = radius > 0
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.lightGray.cgColor
        self.clipsToBounds = true
      //  self.titleLabel?.textColor  = UIColor(hexString: ColorCode.buttontextColor)
        self.setTitleColor(.white, for: .highlighted)
        self.setTitleColor(.white, for: .selected)
        self.setTitleColor(.gray, for: .normal)
    }
    
//    @IBInspectable var borderColor : UIColor = UIColor.lightGray {
//        didSet {
//            self.setNeedsDisplay()
//        }
//    }

    
    //
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //        layer.borderColor = UIColor.clear.cgColor
    //        layer.backgroundColor = UIColor(hexString: ColorCode.themeColor)?.cgColor
    //        self.titleLabel?.textColor = .white
    //
    //    }
    //
    //    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    //        layer.borderColor = borderColor.cgColor
    //        layer.backgroundColor = UIColor.white.cgColor
    //        self.titleLabel?.textColor = .gray
    //    }
    
    
    //    @IBInspectable var imageHighlightColor: UIColor? {
    //        didSet {
    //            self.setImageColorForState(image: (self.imageView?.image!)!, color: imageHighlightColor!, forState: UIControlState.highlighted)
    //        }
    //    }
}

class LabelLayerSetup: UILabel{
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0{
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0{
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var masksToBounds: Bool = false{
        didSet {
            layer.masksToBounds = masksToBounds
        }
    }
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0
    
//    override func drawText(in rect: CGRect) {
//        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
//        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
//    }
    
    override var intrinsicContentSize: CGSize {
        var intrinsicSuperViewContentSize = super.intrinsicContentSize
        intrinsicSuperViewContentSize.height += topInset + bottomInset
        intrinsicSuperViewContentSize.width += leftInset + rightInset
        return intrinsicSuperViewContentSize
    }
}


class ImageLayerSetup: UIImageView{
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    //    @IBInspectable var shouldRasterize: Bool = false{
    //        didSet{
    //            layer.shouldRasterize = shouldRasterize
    //        }
    //    }
    
    @IBInspectable var shadowRadius: CGFloat = 0{
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0{
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        didSet {
            layer.shadowColor = shadowColor?.cgColor
        }
    }
    
    @IBInspectable var masksToBounds: Bool = false{
        didSet {
            layer.masksToBounds = masksToBounds
        }
    }
    
    @IBInspectable var shadowOffset: CGSize = CGSize.zero{
        didSet {
            layer.shadowOffset = shadowOffset
        }
    }
    
    // Add new variable
    @IBInspectable var isAllowCircular: Bool = false
    
    override func layoutSubviews() {
        if isAllowCircular {
            super.layoutSubviews()
            let radius: CGFloat = self.bounds.size.width / 2.0
            self.layer.cornerRadius = radius
            self.clipsToBounds = true
        }
    }
}

@IBDesignable class IQTextView: UITextView {
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    func setup() {
        textContainerInset = UIEdgeInsets.zero
        textContainer.lineFragmentPadding = 0
    }
}
