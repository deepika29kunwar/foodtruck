//
//  URLConstants.swift
//  app
//
//  UCash
//
//  Created by Sagar.Gupta on 05/06/17.
//  Copyright © 2017 Sagar.Gupta. All rights reserved.
//

import UIKit

internal final class URLConstants: NSObject {
    // Do not allow instantiation of the class
    private override init() {}
    
    /// Base URL
    private class var baseURL: String {
        
    //   return "http://bandesoft.com:8069/xmlrpc/"
      return "http://foodtruckapp.bandesoft.com:8069/xmlrpc/"
     //   return "foodtruckapp.bandesoft.com:8069/"
    }
    
    private class var mainURL: String{
        return "foodtruckapp.bandesoft.com:8069/xmlrpc/2/object"
        //return "http://foodtruckapp.bandesoft.com:8069/web/"
    }
    
    /// image base url
     class var imageBaseURL: String {
        
        return ""
    }
    
    /// User login
    class var login: String {
        return baseURL + "2/common"
    }
    
    /// User signup
    class var signup: String {
        return baseURL + "2/object"
    }
    
    /// User profile
    class var profile: String {
        return "http://foodtruckapp.bandesoft.com:8069/xmlrpc/2/object"
      //  return "foodtruckapp.bandesoft.com:8069/xmlrpc/2/object"
    }
    
    /// Forgot password
    class var forgotpassword: String {
        return mainURL + "reset_pass"
    }
    
    /// Logout password
    class var logout: String {
        return baseURL + ""
    }
    
    /// google login
    class var googleLogin: String {
        return mainURL + "google_login"
    }
    
    /// edit profile
    class var editprofile: String {
        return baseURL + ""
    }

    /// change password
    class var changepassword: String {
        return baseURL + ""
    }
    
}
