//
//  RequestParams.swift
//  FoodTruckApp
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import Foundation

internal final class ReqestParams: NSObject {
    
    // Do not allow instantiation of the class
    private override init() {}
    
    /// Base URL
    private class var baseURL: String {
        
        return ""
    }
    
 //===========================
    // MARK: - Login Request
 //===========================
class func login(email:String,password:String) -> String {
        let strLogin = """
 <methodCall>
    <methodName>login</methodName> <!-- method name -->
    <params>
        <param>
            <value><string>foodtruckapp</string></value> <!-- database name -->
        </param>
        <param>
            <value><string>\(email)</string></value> <!-- username -->
        </param>
        <param>
            <value><string>\(password)</string></value> <!-- user’s password -->
        </param>
       </params>
 </methodCall>
 """
        return strLogin
    }
    
    //===========================
    // MARK: - Login Authentication Request
    //===========================
    class func loginAuth() -> String {
        let strLogin = """
        <methodCall>
        <methodName>login</methodName> <!-- method name -->
        <params>
        <param>
        <value><string>foodtruckapp</string></value> <!-- database name -->
        </param>
        <param>
        <value><string>app@bandesoft.com</string></value> <!-- username -->
        </param>
        <param>
        <value><string>mass121!</string></value> <!-- user’s password -->
        </param>
        </params>
        </methodCall>
        """
        return strLogin
    }
    //Username and Password for Authentication
    //app@bandesoft.com
    //mass121!
    
    //===========================
    // MARK: - Signup Request
    //===========================
    
    class func signup(username:String,mobile:String,email:String,password:String) -> String {
        let strLogin =
        """
        <methodCall>
        <methodName>execute</methodName>
        <params>
            <param>
                <!-- database name -->
                <value><string>foodtruckapp</string></value>
            </param>
            <param>
                <!-- user id -->
                <value><int>\(FoodTruckManager.share.commonid)</int></value>
            </param>
            <param>
                <!-- user’s password-->
                <value>
                    <string>admin</string>
                </value>
            </param>
            <param>
                <!-- model name -->
                <value>
                    <string>res.users</string>
                </value>
            </param>
            <param>
                <!-- method name -->
                <value>
                    <string>signup</string>
                </value>
            </param>
            <param>
                <value>
                    <struct>
                        <member>
                            <name>name</name> <!-- field name -->
                            <value>
                                <!-- field’s type and value -->
                                <string>\(username)</string>
                            </value>
                        </member>
                        <member>
                            <name>login</name> <!-- field name -->
                            <value>
                                <!-- field’s type and value -->
                                <string>\(email)</string>
                            </value>
                        </member>
                        <member>
                            <name>password</name> <!-- field name -->
                            <value>
                                <!-- field’s type and value -->
                                <string>\(password)</string>
                            </value>
                        </member>
                    </struct>
                </value>
            </param>
        </params>
        </methodCall>
        """
        return strLogin
    }
    
    
    //===========================
    // MARK: - Reset Password Request
    //===========================
    class func resetPassword(login:String) -> String {
        let strLogin = ""
        return strLogin
    }
    
    
    //===========================
    // MARK: - Create Profile Request
    //===========================
 class func createProfile(name:String,mobile:String,email:String,password:String) -> String {
    let strProfile =
"""
    <methodCall>
        <!-- used to call methods of odoo models -->
        <methodName>execute</methodName>
        <params>
            <param>
                <!-- database name -->
                <value><string>foodtruckapp</string></value>
            </param>
            <param>
                <!-- user id -->
                   <value><int>9</int></value>
            </param>
            <param>
                <!-- password -->
                   <value><string>adarsh121!</string></value>
            </param>
               <param>
                 <!-- model name -->
                   <value><string>res.partner</string></value>
            </param>
            <param>
                <!-- method name -->
                <value><string>write</string></value>
            </param>
            <param>
                <!-- record id of which data you want to update-->
                <value><int>11</int></value>
            </param>
            <param>
                <value>
                    <struct>
                        <member>
                            <name>name</name> <!-- field name -->
                            <!-- field’s value-->
                            <value><string>\(name)</string></value>
                       </member>
                   </struct>
                </value>
            </param>
        </params>
    </methodCall>
 """
    return strProfile
    }
    
    
    //===========================
    // MARK: - Create Location Request
    //===========================
    class func createLocation(name:String,startTime:String,endTime:String,price:String) -> String {
        let strProfile =
        """
        <methodCall>
        <!-- used to call methods of odoo models -->
        <methodName>execute</methodName>
            <params>
                <param>
                    <!-- database name -->
                    <value><string>foodtruckapp</string></value>
                </param>
                <param>
                    <!-- user id -->
                    <value><int>9</int></value>
                </param>
                <param>
                    <!-- user’s password-->
                    <value>
                        <string>adarsh121!</string>
                    </value>
                </param>
                <param>
                    <!-- model name -->
                    <value>
                        <string>location.location</string>
                    </value>
                </param>
                <param>
                    <!-- method name -->
                    <value>
                        <string>create</string>
                    </value>
                </param>
                <param>
                    <!-- create a struct where you identify the field name,
                    thefield type and the value you want to insert in it -->
                    <value>
                        <struct>
                            <member>
                                <name>title</name> <!-- field name -->
                                <value>
                                    <!-- field’s type and value -->
                                    <string>\(name)</string>
                                </value>
                            </member>
                            <member>
                                <name>start_time</name>
                                <value>
                                    <string>\(startTime)</string>
                                </value>
                            </member>
                            <member>
                                <name>end_time</name>
                                <value>
                                    <string>\(endTime)</string>
                                </value>
                            </member>
                            <member>
                                <name>price</name>
                                <value>
                                    <string>\(price)</string>
                                </value>
                            </member>
                        </struct>
                    </value>
                </param>
            </params>
        </methodCall>
        """
        return strProfile
    }
}

