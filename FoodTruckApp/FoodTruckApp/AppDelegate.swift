//
//  AppDelegate.swift
//  FoodTruckApp
//
//  Created by MacBook Pro on 27/07/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate{
    var window: UIWindow?
    var navigationController:UINavigationController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //Initialize sign-in
        IQKeyboardManager.shared.enable = true
        GIDSignIn.sharedInstance().clientID = "312721855828-4ao92ujusj6ftc4t6dbkccav94g6dhsb.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        GMSPlacesClient.provideAPIKey("AIzaSyAj1D6XgIxIym08F5bwFLZVcFBqBqv46aw")
        
        self.rootViewController()
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    //MARK: Google Signin
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            // ...
            
            print("\(fullName)")
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    //MARK: REturnURl
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        if url.absoluteString.contains("fb2147269272264785") {
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
        }
        else {
            return GIDSignIn.sharedInstance().handle(url as URL!, sourceApplication: sourceApplication, annotation: annotation)
        }
    }
    
    func rootViewController(){
        let userid = ApplicationPreference.getUserId()
        if (userid != nil) {
        let storyBoard = UIStoryboard(name: AppConstant.homestorybaord, bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RootViewController") as! RootViewController
        self.navigationController  = UINavigationController(rootViewController: viewController)
        self.window?.rootViewController = self.navigationController
        self.navigationController?.isNavigationBarHidden = true
        self.window?.makeKeyAndVisible()
        }else{
            let storyBoard = UIStoryboard(name: AppConstant.onBoardstorybaord, bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController  = UINavigationController(rootViewController: viewController)
            self.window?.rootViewController = self.navigationController
            self.navigationController?.isNavigationBarHidden = true
            self.window?.makeKeyAndVisible()
        }
    }
}

