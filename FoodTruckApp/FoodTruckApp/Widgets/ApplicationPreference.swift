//
//  ApplicationPreference.swift
//  Ikan
//

import Foundation

class ApplicationPreference{
    
    fileprivate static let defaults = UserDefaults.standard
    
    //////////////// Remove all info //////
    class func clearAllData(){
        defaults.removeObject(forKey: AppConstant.userDefaultUserId)
        defaults.synchronize()
    }
    
    ///////////////// GET & SAVE UserId //////
    class func saveUserId(userId: String){
        defaults.set(userId, forKey: AppConstant.userDefaultUserId)
        defaults.synchronize()
    }
    
    class func getUserId()->String?{
        let userId:String?
        userId = defaults.object(forKey: AppConstant.userDefaultUserId) as? String
        return userId ?? nil
    }
   
    
    ///////////////// GET & SAVE UserId //////
    class func saveProfileId(profileId: String){
        defaults.set(profileId, forKey: AppConstant.userDefaultProfileId)
        defaults.synchronize()
    }
    
    class func getProfileId()->String?{
        let profileId:String?
        profileId = defaults.object(forKey: AppConstant.userDefaultProfileId) as? String
        return profileId ?? nil
    }
}
