//
//  DateTimeUtils.swift
//  Ikan
//


import Foundation

class DateTimeUtils {
    
    static let sharedInstance = DateTimeUtils()

    private var DF_TIME_hh_mm_aa:DateFormatter?
    private var DF_TIME_yyyy_MM_dd:DateFormatter?
    private var DF_TIME_dd_MMM_yy:DateFormatter?
    private var DF_TIME_dd_MMMM_yyyy:DateFormatter?
    private var DF_TIME_dd_MMM_yyyy:DateFormatter?
    private var DF_TIME_dd_MMM:DateFormatter?
    private var DF_dd_MM_yyyy:DateFormatter?
    private var DF_DAY_MMM_dd_yyyy:DateFormatter?
    private var DF_MMMM:DateFormatter?
    private var DF_FULL_DAY_DD_MM_YYYY:DateFormatter?
    private var DF_dd_MM_yyyy_TIME:DateFormatter?
    private var DF_dd_MM_yyyy_TIME_12_HOURS:DateFormatter?
    private var DF_dd_MM_TIME_12_HOURS:DateFormatter?
    
    private var DF_MMM_dd_yyyy:DateFormatter?
    
    private var DF_dd_mm_yyyy:DateFormatter?
    
    init(){
        loadFormatter()
    }
    
    func loadFormatter() {
        
        DF_TIME_hh_mm_aa = DateFormatter()
        if(DF_TIME_hh_mm_aa != nil){
            DF_TIME_hh_mm_aa?.dateFormat = "hh:mm a"
            DF_TIME_hh_mm_aa?.timeZone = TimeZone.current
            DF_TIME_hh_mm_aa?.locale = Locale.current
        }
        
        
        DF_TIME_dd_MMMM_yyyy = DateFormatter()
        if(DF_TIME_dd_MMMM_yyyy != nil){
            DF_TIME_dd_MMMM_yyyy?.dateFormat = "dd-MMMM-yyyy"
            DF_TIME_dd_MMMM_yyyy?.timeZone = TimeZone.current
            DF_TIME_dd_MMMM_yyyy?.locale = Locale.current
        }
        
        DF_TIME_dd_MMM_yyyy = DateFormatter()
        if(DF_TIME_dd_MMM_yyyy != nil){
            DF_TIME_dd_MMM_yyyy?.dateFormat = "dd-MMM-yyyy"
            DF_TIME_dd_MMM_yyyy?.timeZone = TimeZone.current
            DF_TIME_dd_MMM_yyyy?.locale = Locale.current
        }
        
        DF_TIME_dd_MMM = DateFormatter()
        if(DF_TIME_dd_MMM != nil){
            DF_TIME_dd_MMM?.dateFormat = "dd-MMMM"
            DF_TIME_dd_MMM?.timeZone = TimeZone.current
            DF_TIME_dd_MMM?.locale = Locale.current
        }
        
        DF_TIME_yyyy_MM_dd = DateFormatter()
        if(DF_TIME_yyyy_MM_dd != nil){
            DF_TIME_yyyy_MM_dd?.dateFormat = "yyyy/MM/dd"
            DF_TIME_yyyy_MM_dd?.timeZone = TimeZone.current
            DF_TIME_yyyy_MM_dd?.locale = Locale.current
        }
        
        DF_TIME_dd_MMM_yy = DateFormatter()
        if(DF_TIME_dd_MMM_yy != nil){
            DF_TIME_dd_MMM_yy?.dateFormat = "dd/MMM/yy"
            DF_TIME_dd_MMM_yy?.timeZone = TimeZone.current
            DF_TIME_dd_MMM_yy?.locale = Locale.current
        }

        DF_DAY_MMM_dd_yyyy = DateFormatter()
        if(DF_DAY_MMM_dd_yyyy != nil){
            DF_DAY_MMM_dd_yyyy?.dateFormat = "EEE, dd MMM, yyyy"
            DF_DAY_MMM_dd_yyyy?.timeZone = TimeZone.current
            DF_DAY_MMM_dd_yyyy?.locale = Locale.current
        }
        
        DF_MMMM = DateFormatter()
        if(DF_MMMM != nil){
            DF_MMMM?.dateFormat = "MMMM yyyy"
            DF_MMMM?.timeZone = TimeZone.current
            DF_MMMM?.locale = Locale.current
        }
        
        DF_FULL_DAY_DD_MM_YYYY = DateFormatter()
        if(DF_FULL_DAY_DD_MM_YYYY != nil){
            DF_FULL_DAY_DD_MM_YYYY?.dateFormat = "EEEE, dd MMMM, yyyy"
            DF_FULL_DAY_DD_MM_YYYY?.timeZone = TimeZone.current
            DF_FULL_DAY_DD_MM_YYYY?.locale = Locale.current
        }
        
        DF_dd_MM_yyyy = DateFormatter()
        if(DF_dd_MM_yyyy != nil){
            DF_dd_MM_yyyy?.dateFormat = "dd/MM/yyyy"
            DF_dd_MM_yyyy?.timeZone = TimeZone.current
            DF_dd_MM_yyyy?.locale = Locale.current
        }
        
        DF_dd_MM_yyyy_TIME  = DateFormatter()
        if(DF_dd_MM_yyyy_TIME != nil){
            DF_dd_MM_yyyy_TIME?.dateFormat = "dd/MM/yyyy HH:mm:ss z"
            DF_dd_MM_yyyy_TIME?.timeZone = TimeZone.current
            DF_dd_MM_yyyy_TIME?.locale = Locale.current
        }
        
        DF_dd_MM_yyyy_TIME_12_HOURS = DateFormatter()
        if(DF_dd_MM_yyyy_TIME_12_HOURS != nil){
            //DF_dd_MM_yyyy_TIME_12_HOURS?.dateFormat = "dd/MM/yyyy HH:mm a"
            DF_dd_MM_yyyy_TIME_12_HOURS?.dateFormat = "dd/MM/yyyy HH:mm"
            DF_dd_MM_yyyy_TIME_12_HOURS?.timeZone = TimeZone.current
            DF_dd_MM_yyyy_TIME_12_HOURS?.locale = Locale.current
        }
        
        DF_dd_MM_TIME_12_HOURS = DateFormatter()
        if(DF_dd_MM_TIME_12_HOURS != nil){
            //DF_dd_MM_TIME_12_HOURS?.dateFormat = "dd/MM/yyyy HH:mm a"
            DF_dd_MM_TIME_12_HOURS?.dateFormat = "dd-MMM HH:mm"
            DF_dd_MM_TIME_12_HOURS?.timeZone = TimeZone.current
            DF_dd_MM_TIME_12_HOURS?.locale = Locale.current
        }
        
        DF_MMM_dd_yyyy = DateFormatter()
        if(DF_MMM_dd_yyyy != nil){
            DF_MMM_dd_yyyy?.dateFormat = "MMM dd, yyyy"
            DF_MMM_dd_yyyy?.timeZone = TimeZone.current
            DF_MMM_dd_yyyy?.locale = Locale.current
        }
        
        DF_dd_mm_yyyy = DateFormatter()
        if(DF_dd_mm_yyyy != nil){
            DF_dd_mm_yyyy?.dateFormat = "dd MMM yyyyy"
            DF_dd_mm_yyyy?.timeZone = TimeZone.current
            DF_dd_mm_yyyy?.locale = Locale.current
        }
    }
    
    func formateDateForNotification(date:Date) ->String? {
        return (DF_dd_mm_yyyy?.string(from: date))!
    }

    func currentDateForConfirRX(date:Date) ->String? {
        return (DF_MMM_dd_yyyy?.string(from: date))!
    }
    
    func formatSimpleTime(date:Date) ->String? {
        return (DF_TIME_hh_mm_aa?.string(from: date))!
    }

    func formatFancyDate(date:Date) ->String? {
        return (DF_TIME_dd_MMMM_yyyy?.string(from: date))!
    }
    
    func formatFancyShortDate(date:Date) ->String? {
        return (DF_TIME_dd_MMM_yyyy?.string(from: date))!
    }
    
    func formatFancyDateForHistory(date:Date) ->String? {
        return (DF_TIME_dd_MMM?.string(from: date))!
    }
    func  formatSimpleDate(date:Date) -> String?{
        return (DF_TIME_dd_MMM_yy?.string(from: date))!
    }
    
    func formatDateToString(date:Date) -> String{
        return (DF_TIME_yyyy_MM_dd?.string(from: date))!
    }
    
    func formateDateToMonthString(date:Date) -> String{
        return (DF_MMMM?.string(from: date))!
    }
    
    func formateFullDateStr(date:Date) -> String{
        return (DF_FULL_DAY_DD_MM_YYYY?.string(from:date))!
    }
    
    func formateDateTimeToString(date:Date) -> String{
        return (DF_dd_MM_yyyy_TIME?.string(from:date))!
    }
    
    func formateDateTimeTo12HourTimeString(date:Date) -> String{
        return (DF_dd_MM_yyyy_TIME_12_HOURS?.string(from:date))!
    }
    
    func formateShortDateTimeTo12HourTimeString(date:Date) -> String{
        return (DF_dd_MM_TIME_12_HOURS?.string(from: date))!
    }

    
    func dateFromServerStr(dateStr:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss z"
        let convertedStr = dateStr + " 00:00:00 z"
        let convertedDate = dateFormatter.date(from:convertedStr)
        return convertedDate
    }
    
    ///////   UTC /////
    func localToUTC(date:String, fromFormat: String, toFormat: String) -> Int {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = toFormat
        
        let someDate = dt
        let timeInterval = (someDate?.timeIntervalSince1970)! * 1000
        let myInt = Int(timeInterval)
        
        return myInt
        //return dateFormatter.string(from: dt!)
    }
    
    func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "MMM dd, yyyy"
        
        return dateFormatter.string(from: dt!)
    }
    
    ////MINE
    func localToUTC(date:String) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: date)
        
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let someDate = dt
        
        // convert Date to TimeInterval (typealias for Double)
        let timeInterval = (someDate?.timeIntervalSince1970)! * 1000
        
        // convert to Integer
        let myInt = Int(timeInterval)
        
        return myInt
    }
    
    func convertToUTC(dateToConvert:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let convertedDate = formatter.date(from: dateToConvert)
        formatter.timeZone = TimeZone(identifier: "UTC")
        return formatter.string(from: convertedDate!)
        
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.string(from: dt!)
    }

    func dateForCustomLog(dateStr:String, timeStr:String) -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a"
        let convertedStr = dateStr + " " + timeStr
        let convertedDate = dateFormatter.date(from:convertedStr)
        return convertedDate
    }
    
    func formatDayDateFormat(date:Date) -> String?{
        return (DF_DAY_MMM_dd_yyyy?.string(from: date))!
    }

    func getDeviceTimeZoneDateStrFromUnixFormat(_ dateUnixFormate:Double) -> String?{
        let date = Date(timeIntervalSince1970: dateUnixFormate / 1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd h:mm a" //Specify your format that you want
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
//    func getHrsMinFromDateToCurrentDate(_ dateUnixFormate:Double) -> (Int, Int){
//        let date = Date(timeIntervalSince1970: dateUnixFormate)
//        //let serverTimeSeconds = Int(ApplicationPreference.getServerTime()!)!
//
//        let startTimeStamp = Int(date.timeIntervalSince1970)*1000
//        let endTimeStamp = Int(((Date().timeIntervalSince1970)*1000)) + (serverTimeSeconds)
//        let totalSeconds = (endTimeStamp - startTimeStamp)/1000
//
//        let hrs = (totalSeconds/60)/60
//        let mins: Int = (Int(round(Double(totalSeconds))) / 60) % 60
//
//        return (Int(hrs), Int(mins))
//    }
//
    func getDeviceTimeZoneTimeStrFromUnixFormat(_ dateUnixFormate:Double) -> String?{
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "hh:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func getCurrentMonthName() -> String{
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        let nameOfMonth = dateFormatter.string(from: now)
        return nameOfMonth
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60)
    }
    
    func dateFromStr(dateStr:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss z"
        //let convertedStr = dateStr + " 00:00:00 z"
        let convertedStr = dateStr
        let convertedDate = dateFormatter.date(from:convertedStr)
        return convertedDate
    }
    func fancyDateFromStr(dateStr:String) -> Date? {
        let convertedStr = dateStr
        let convertedDate = DF_TIME_dd_MMMM_yyyy?.date(from:convertedStr)
        return convertedDate
    }
    
    func fancyShortDateFromStr(dateStr:String) -> Date? {
        let convertedStr = dateStr
        let convertedDate = DF_TIME_dd_MMM_yyyy?.date(from:convertedStr)
        return convertedDate
    }
    
    func todayDateWithStartTime() -> Date?{
        let todayDate = Date()
        let todayDateStr = DF_dd_MM_yyyy?.string(from: todayDate)
        let todayDateWithTimeStr = "\(todayDateStr!) 00:00:00 z"
        
        return dateFromStr(dateStr: todayDateWithTimeStr)
    }
    
    func todayDateWithEndTime() -> Date?{
        let todayDate = Date()
        let todayDateStr = DF_dd_MM_yyyy?.string(from: todayDate)
        let todayDateWithTimeStr = "\(todayDateStr!) 23:59:59 z"
        
        return dateFromStr(dateStr: todayDateWithTimeStr)
    }
    
    func getDateFromTimeInterval(timeInterval:TimeInterval) -> Date{
        return Date(timeIntervalSince1970: timeInterval / 1000.0)
    }
    
    func getTimeIntervalFromDate(date:Date) -> TimeInterval{
        return (date.timeIntervalSince1970)*1000
    }
    
    func getimeIntervalFromDate(timeInterval:Date) -> String{
        return String(describing: Date.timeIntervalSince(timeInterval))
    }
    
//    func getDateFromStringTimeInterval(timeInterval:String) -> Date?{
//        let dateTimeMilliSecondString:String? = BaseApp.sharedInstance.removeOptionalWordFromString(timeInterval)
//
//        if(dateTimeMilliSecondString != nil){
//            let messageDate:Date? = self.getDateFromTimeInterval(timeInterval: Double(dateTimeMilliSecondString!)!)
//            return messageDate
//        }
//        return nil
//    }

}

extension Date {
    
    /// Returns a Date with the specified days added to the one it is called with
    func add(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        var targetDay: Date
        targetDay = Calendar.current.date(byAdding: .year, value: years, to: self)!
        targetDay = Calendar.current.date(byAdding: .month, value: months, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .day, value: days, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .hour, value: hours, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .minute, value: minutes, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .second, value: seconds, to: targetDay)!
        return targetDay
    }
    
    /// Returns a Date with the specified days subtracted from the one it is called with
    func subtract(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        let inverseYears = -1 * years
        let inverseMonths = -1 * months
        let inverseDays = -1 * days
        let inverseHours = -1 * hours
        let inverseMinutes = -1 * minutes
        let inverseSeconds = -1 * seconds
        return add(years: inverseYears, months: inverseMonths, days: inverseDays, hours: inverseHours, minutes: inverseMinutes, seconds: inverseSeconds)
    }
    
}
