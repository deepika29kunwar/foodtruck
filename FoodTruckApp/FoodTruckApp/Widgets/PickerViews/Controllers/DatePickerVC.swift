//
//  DatePickerVC.swift
//  MD
//
//  Created by Mahesh Dhakad on 11/02/18.
//  Copyright © 2018 MD. All rights reserved.
//

import UIKit

enum DatePickerType{
    case DateTime
    case Date
    case Time
}

class DatePickerVC: UIViewController {
    
    
    //MARK: - var
    //MARK: -
    
    public var onSelectDone:(String, Date)->() = {_,_ in}
   
    var preSelectedDate = Date();
    var selectDate = "";
    var datePickerType:DatePickerType = .DateTime
    var minimumDate = Date().adding(days: -1);
    var maximumDate = Date().adding(days: -1);

    //MARK: - @IBOutlet
    //MARK: -
    @IBOutlet weak var viewPopUp: UIView!
    @IBOutlet weak var btn_Shadow: UIButton!
    @IBOutlet weak var blurView: UIView!


    @IBOutlet var dataPicker: UIDatePicker!
    @IBOutlet var lbl_CalenderHeader: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
        
        let formatter = DateFormatter()

        switch datePickerType {
        case .DateTime:
            print("DateTime")
            dataPicker.datePickerMode = .dateAndTime
            dataPicker.date = preSelectedDate
            dataPicker.minimumDate = minimumDate
            dataPicker.maximumDate = maximumDate
            
            formatter.dateFormat = "dd-MMM-YYYY hh:mm:ss a"
            lbl_CalenderHeader.text = "Select Date & Time"
            break;

        case .Date:
            print("Date")
            dataPicker.datePickerMode = .date
            dataPicker.date = preSelectedDate
            dataPicker.minimumDate = minimumDate
            dataPicker.maximumDate = maximumDate
            
            formatter.dateFormat = "EEEE, dd-MMM-YYYY"

           /* // Set Minimum & Maximum Data on Date Picker
            let calendar = NSCalendar.init(calendarIdentifier: .gregorian)
            let currentDat = Date()
            var comps = DateComponents()
            comps.year = 0
           // let maxDate: Date? = calendar?.date(byAdding: comps, to: currentDat, options: [])
            let minDate: Date? = calendar?.date(byAdding: comps, to: currentDat, options: [])
            comps.year = 0
           //dataPicker.maximumDate = maxDate
            dataPicker.minimumDate = minDate
            
            // Set Data On Header into String Formatte
            let completeDate = "\(formatter.string(from: Date()))"
            lbl_CalenderHeader.text = completeDate */
            
            lbl_CalenderHeader.text = "Select Date"
            
            break;

        case .Time:
            print("Time")
            dataPicker.datePickerMode = .time
            dataPicker.date = preSelectedDate
            dataPicker.minimumDate = minimumDate

            formatter.dateFormat = "hh:mm a"
            lbl_CalenderHeader.text = "Select Time"
            break;
        }
        
       // dataPicker.date = Date()
        dataPicker.backgroundColor = UIColor.white
        dataPicker.tintColor = UIColor.blue
        
       // dataPicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // UIApplication.setStatusBarBackgroundColor(color:.clear)
        
        self.viewPopUp.transform = CGAffineTransform(scaleX: 0.01, y: 0.01);
        
        UIView.animate(withDuration: 0.25 ,
                       animations: {
                        self.viewPopUp.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
                        self.viewPopUp.alpha = 1
        },completion: { finish in
            self.viewPopUp.isHidden = false
            self.btn_Shadow?.isHidden = false
            self.blurView?.isHidden = false

            UIView.animate(withDuration: 0.15){
                self.viewPopUp.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                UIView.animate(withDuration: 0.15){
                    self.viewPopUp.transform = CGAffineTransform.identity
                }
            }
        })
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func datePicker_ValueChange(_ sender: Any) {
        selected_Date()
    }
    
    func selected_Date() {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd-MMM-YYYY"
        selectDate = formatter.string(from: dataPicker.date)
       // lbl_CalenderHeader.text = selectDate
    }
    
    // MARK: - IBAction Cancel / Done
    // MARK: -
    
    @IBAction func action_Cancel(_ sender: Any) {
        
        UIView.animate(withDuration: 0.1 ,
                       animations: {
                        self.viewPopUp.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
        },completion: { finish in
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveLinear, animations: {
                self.viewPopUp.alpha = 0.0
                self.viewPopUp.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            }, completion: { (finished) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.viewPopUp.alpha = 0.0
                }, completion: { (finished) in
                    self.viewPopUp.isHidden = true;
                    self.btn_Shadow?.isHidden = true
                    self.blurView?.isHidden = true
                    self.dismiss(animated: false) {}
                    
                })
            })
        })
    }
    
    
    @IBAction func action_Done(_ sender: Any) {
        
        let format = DateFormatter()
        format.dateFormat = "dd-MMM-YYYY, hh:mm a"
        selectDate = format.string(from: dataPicker.date)
        dataPicker.reloadInputViews()
        let timeStamp = DateTimeUtils.sharedInstance.getTimeIntervalFromDate(date: dataPicker.date)

        self.onSelectDone("\(Int(timeStamp)/1000)", dataPicker.date)
        
        UIView.animate(withDuration: 0.1 ,
                       animations: {
                        self.viewPopUp.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                        
        },completion: { finish in
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveLinear, animations: {
                self.viewPopUp.alpha = 0.0
                self.viewPopUp.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            }, completion: { (finished) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.viewPopUp.alpha = 0.0
                }, completion: { (finished) in
                    self.viewPopUp.isHidden = true;
                    self.btn_Shadow?.isHidden = true
                    self.blurView?.isHidden = true
                    self.dismiss(animated: false) {}
                    
                })
            })
        })
    }
    
    @IBAction func action_Close(_ sender: Any) {

        UIView.animate(withDuration: 0.1 ,
                       animations: {
                        self.viewPopUp.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
        },completion: { finish in
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveLinear, animations: {
                self.viewPopUp.alpha = 0.0
                self.viewPopUp.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            }, completion: { (finished) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.viewPopUp.alpha = 0.0
                }, completion: { (finished) in
                    self.viewPopUp.isHidden = true;
                    self.btn_Shadow?.isHidden = true
                    self.blurView?.isHidden = true
                    self.dismiss(animated: false) {}
                    
                })
            })
        })
        
    }
    
    // MARK: - @end
    // MARK: -
    

}


extension Date{
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func adding(days: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
    
    func adding(months: Int) -> Date {
        return Calendar.current.date(byAdding: .month, value: months, to: self)!
    }
}
