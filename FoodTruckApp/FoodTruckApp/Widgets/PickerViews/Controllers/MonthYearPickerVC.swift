//
//  MonthYearPickerVC.swift
//  MD
//
//  Created by Mahesh Dhakad on 11/02/18.
//  Copyright © 2018 MD. All rights reserved.
//

import UIKit

class MonthYearPickerVC: UIViewController {
    
    
    //MARK: - var
    //MARK: -
    public var pickerType = "MONTHS_YEARS"
   // public var onSelectDone:(String) -> () = {_ in}
    public var onSelectDone: ((_ selected: String) -> Void)?

    //MARK: - @IBOutlet
    //MARK: -
    
    @IBOutlet weak var viewPopUp: UIView!
    @IBOutlet weak var btn_Shadow: UIButton!
    @IBOutlet weak var blurView: UIView!


    @IBOutlet var pickerView: MonthYearPickerView!
    @IBOutlet var lbl_CalenderHeader: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if pickerType == PickerType.MONTHS_YEARS.rawValue {
            self.lbl_CalenderHeader?.text = "Select Month & Year"
        } else if pickerType == PickerType.YEARS.rawValue {
            self.lbl_CalenderHeader?.text = "Select Year"
        } else {
            self.lbl_CalenderHeader?.text = "Please Select"
        }
        
        pickerView.pickerType = pickerType

        pickerView.onSelectedMonthYear = { (month: Int, year: Int) in
            let string = String(format: "%02d/%d", month, year)
            NSLog(string) // should show something like "03/2018"
            self.lbl_CalenderHeader?.text = string
        }
        
        pickerView.onSelectedYear = { (year: Int) in
            let string = String(format: "%d", year)
            NSLog(string) // should show something like "2018"
            self.lbl_CalenderHeader?.text = string
        }
        
        pickerView.onSelectedOther = { (other: String) in
            let string = String(format: "%@", other)
            NSLog(string) // should show something like "Mahesh"
            self.lbl_CalenderHeader?.text = other
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // UIApplication.setStatusBarBackgroundColor(color:.clear)
        
        self.viewPopUp.transform = CGAffineTransform(scaleX: 0.01, y: 0.01);
        
        UIView.animate(withDuration: 0.25 ,
                       animations: {
                        self.viewPopUp.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
                        self.viewPopUp.alpha = 1
        },completion: { finish in
            self.viewPopUp.isHidden = false
            self.btn_Shadow?.isHidden = false
            self.blurView?.isHidden = false

            UIView.animate(withDuration: 0.15){
                self.viewPopUp.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                UIView.animate(withDuration: 0.15){
                    self.viewPopUp.transform = CGAffineTransform.identity
                }
            }
        })
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - IBAction Cancel / Done
    // MARK: -
    
    @IBAction func action_Cancel(_ sender: Any) {
        
        UIView.animate(withDuration: 0.1 ,
                       animations: {
                        self.viewPopUp.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
        },completion: { finish in
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveLinear, animations: {
                self.viewPopUp.alpha = 0.0
                self.viewPopUp.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            }, completion: { (finished) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.viewPopUp.alpha = 0.0
                }, completion: { (finished) in
                    self.viewPopUp.isHidden = true;
                    self.btn_Shadow?.isHidden = true
                    self.blurView?.isHidden = true
                    self.dismiss(animated: false) {}
                    
                })
            })
        })
    }
    
    
    @IBAction func action_Done(_ sender: Any) {
        
        if let txt = self.lbl_CalenderHeader?.text, !txt.contains("Select") {
            
            if let block = onSelectDone {
                block("\(txt)")
            }
        } else {
            
            if pickerType == PickerType.MONTHS_YEARS.rawValue {
                let month = pickerView.selectedRow(inComponent: 0)+1
                let year = pickerView.pickerView(pickerView, titleForRow: 0, forComponent: 1)
                self.lbl_CalenderHeader?.text = "\(month)/\(year!)"
            } else if pickerType == PickerType.YEARS.rawValue {
                let year = pickerView.pickerView(pickerView, titleForRow: 0, forComponent: 0)
                self.lbl_CalenderHeader?.text = "\(year!)"
            } else {
                let other = pickerView.pickerView(pickerView, titleForRow: 0, forComponent: 0)
                self.lbl_CalenderHeader?.text = "\(other!)"
            }
            if let txt = self.lbl_CalenderHeader?.text, !txt.contains("Select") {
                
                if let block = onSelectDone {
                    block("\(txt)")
                }
            }
        }
        
        UIView.animate(withDuration: 0.1 ,
                       animations: {
                        self.viewPopUp.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                        
        },completion: { finish in
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveLinear, animations: {
                self.viewPopUp.alpha = 0.0
                self.viewPopUp.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            }, completion: { (finished) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.viewPopUp.alpha = 0.0
                }, completion: { (finished) in
                    self.viewPopUp.isHidden = true;
                    self.btn_Shadow?.isHidden = true
                    self.blurView?.isHidden = true
                    self.dismiss(animated: false) {}
                    
                })
            })
        })
    }
    
    @IBAction func action_Close(_ sender: Any) {

        UIView.animate(withDuration: 0.1 ,
                       animations: {
                        self.viewPopUp.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
        },completion: { finish in
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveLinear, animations: {
                self.viewPopUp.alpha = 0.0
                self.viewPopUp.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            }, completion: { (finished) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.viewPopUp.alpha = 0.0
                }, completion: { (finished) in
                    self.viewPopUp.isHidden = true;
                    self.btn_Shadow?.isHidden = true
                    self.blurView?.isHidden = true
                    self.dismiss(animated: false) {}
                })
            })
        })
        
    }
    
    // MARK: - @end
    // MARK: -
    
}
