//
//  PickerViewController.swift
//  Restaurant
//
//  Created by MD on 06/04/18.
//  Copyright © 2018 consagous. All rights reserved.
//

import Foundation
import UIKit


var restaurantTypeList: [String]! = []
var restaurantCuisineList: [String]! = []

enum PickerViewType : String {
    case RestaurantType
    case RestaurantCuisine
}

class PickerViewController: UIViewController {
    
    //MARK: - var
    //MARK: -
    public var pickerType: String?
    public var pickedItem: String?

    public var onSelectDone: ((_ selected: String) -> Void)?
    
  
    
    //MARK: - @IBOutlet
    //MARK: -
    @IBOutlet weak var av: UIActivityIndicatorView!

    @IBOutlet weak var viewPopUp: UIView!
    @IBOutlet weak var btn_Shadow: UIButton!
    @IBOutlet weak var blurView: UIView!

    
    @IBOutlet var pickerView: ListPickerView!
    @IBOutlet var lbl_CalenderHeader: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.av?.isHidden = true

        self.lbl_CalenderHeader?.text = "Please Select"
        
        
        if pickerType == PickerViewType.RestaurantType.rawValue {
            
            self.lbl_CalenderHeader?.text = "Select Restaurant Type"

            if restaurantTypeList.count != 0 {
                pickerView.pickerViewList = restaurantTypeList
            } else {
                self.av?.isHidden = false
               // self.serviceAPIRestaurantsType()
            }

        } else if pickerType == PickerViewType.RestaurantCuisine.rawValue {
            
            self.lbl_CalenderHeader?.text = "Select Restaurant Cuisine"

            if restaurantCuisineList.count != 0 {
                pickerView.pickerViewList = restaurantCuisineList
            } else {
                self.av?.isHidden = false
                //self.serviceAPIRestaurantsCuisine()
            }
        } else {
            
        }
        
        pickerView.onSelectedItem = { (item: String) in
            let string = String(format: "%d", item)
            NSLog(string) // should show something like "Mahesh"
           // self.lbl_CalenderHeader?.text = string
            
            if self.pickerType == PickerViewType.RestaurantType.rawValue {
                
                if let index = restaurantTypeList.index(of: item) {
                    let id = Int(index) + 1
                    self.pickedItem = item + ",\(id)"
                }
                
            } else if self.pickerType == PickerViewType.RestaurantCuisine.rawValue {
                
                if let index = restaurantCuisineList.index(of: item) {
                    let id = Int(index) + 1
                    self.pickedItem = item + ",\(id)"
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // UIApplication.setStatusBarBackgroundColor(color:.clear)
        
        self.viewPopUp.transform = CGAffineTransform(scaleX: 0.01, y: 0.01);
        
        UIView.animate(withDuration: 0.25 ,
                       animations: {
                        self.viewPopUp.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
                        self.viewPopUp.alpha = 1
        },completion: { finish in
            self.viewPopUp.isHidden = false
            self.btn_Shadow?.isHidden = false
            self.blurView?.isHidden = false

            UIView.animate(withDuration: 0.15){
                self.viewPopUp.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                UIView.animate(withDuration: 0.15){
                    self.viewPopUp.transform = CGAffineTransform.identity
                }
            }
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
   
    
}
// MARK: - APIs
// MARK: -

/*
extension PickerViewController {
    
    func serviceAPIRestaurantsType() {
        
        if(BaseApp.sharedInstance.isNetworkConnected){
            
            BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
            
            let token = ApplicationPreference.getAppToken()
            let userId = ApplicationPreference.getUserId()
            
            let request = RestaurantTypeRequest(token:token, userId: userId, onSuccess: {
                response in
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    BaseApp.sharedInstance.hideProgressHudView()
                    
                    if(response.restaurantTypeData != nil){
                       // self.restaurantType = response.restaurantTypeData!
                    }
                    
                    for restType in response.restaurantTypeData! {
                        restaurantTypeList.append(restType.type!)
                    }
                    
                    self.pickerView.pickerViewList = restaurantTypeList
                    // Update info on screen
                    self.pickerView.reloadAllComponents()
                    self.av?.isHidden = true

                }
            }, onError: {
                error in
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    BaseApp.sharedInstance.hideProgressHudView()
                    BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: error.errorMsg!, buttonTitle: BaseApp.sharedInstance.getMessageForCode("ok")!, controller: self)
                    self.av?.isHidden = true
                }
            })
            
            BaseApp.sharedInstance.jobManager?.addOperation(request)
            
        } else {
            BaseApp.sharedInstance.showNetworkNotAvailableAlertController()
            self.av?.isHidden = true
        }
        
        
    }
    
    
    func serviceAPIRestaurantsCuisine() {
        
        if(BaseApp.sharedInstance.isNetworkConnected){
            
            BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
            
            let token = ApplicationPreference.getAppToken()
            let userId = ApplicationPreference.getUserId()
            
            let request = RestaurantCuisineRequest(token:token, userId: userId, onSuccess: {
                response in
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    BaseApp.sharedInstance.hideProgressHudView()
                    
                    if(response.restaurantCuisineData != nil){
                       // self.restaurantCuisine = response.restaurantCuisineData!
                    }
                    
                    for restCuisine in response.restaurantCuisineData! {
                        restaurantCuisineList.append(restCuisine.cuisine_name!)
                    }
                    
                    self.pickerView.pickerViewList = restaurantCuisineList
                    // Update info on screen
                    self.pickerView.reloadAllComponents()
                    self.av?.isHidden = true
                }
            }, onError: {
                error in
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    BaseApp.sharedInstance.hideProgressHudView()
                    BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: error.errorMsg!, buttonTitle: BaseApp.sharedInstance.getMessageForCode("ok")!, controller: self)
                    self.av?.isHidden = true

                }
            })
            
            BaseApp.sharedInstance.jobManager?.addOperation(request)
            
        } else {
            BaseApp.sharedInstance.showNetworkNotAvailableAlertController()
            self.av?.isHidden = true

        }
        
        
    }
    
}
*/

// MARK: - IBAction Cancel / Done
// MARK: -

extension PickerViewController {

    @IBAction func action_Cancel(_ sender: Any) {
        
        UIView.animate(withDuration: 0.1 ,
                       animations: {
                        self.viewPopUp.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
        },completion: { finish in
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveLinear, animations: {
                self.viewPopUp.alpha = 0.0
                self.viewPopUp.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            }, completion: { (finished) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.viewPopUp.alpha = 0.0
                }, completion: { (finished) in
                    self.viewPopUp.isHidden = true;
                    self.btn_Shadow?.isHidden = true
                    self.blurView?.isHidden = true
                    self.dismiss(animated: false) {}
                    
                })
            })
        })
    }
    
    
    @IBAction func action_Done(_ sender: Any) {
        
        if let txt = self.pickedItem, txt.count != 0 {
            
            if let block = onSelectDone {
                block("\(txt)")
            }
        } else {
            
            if let block = onSelectDone {
                block("\(self.pickerView.pickerView(self.pickerView, titleForRow: 0, forComponent: 0)!),1")
            }
        }
        
        UIView.animate(withDuration: 0.1 ,
                       animations: {
                        self.viewPopUp.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                        
        },completion: { finish in
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveLinear, animations: {
                self.viewPopUp.alpha = 0.0
                self.viewPopUp.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            }, completion: { (finished) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.viewPopUp.alpha = 0.0
                }, completion: { (finished) in
                    self.viewPopUp.isHidden = true;
                    self.btn_Shadow?.isHidden = true
                    self.blurView?.isHidden = true
                    self.dismiss(animated: false) {}
                    
                })
            })
        })
    }
    
    @IBAction func action_Close(_ sender: Any) {
        
        UIView.animate(withDuration: 0.1 ,
                       animations: {
                        self.viewPopUp.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
        },completion: { finish in
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveLinear, animations: {
                self.viewPopUp.alpha = 0.0
                self.viewPopUp.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            }, completion: { (finished) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.viewPopUp.alpha = 0.0
                }, completion: { (finished) in
                    self.viewPopUp.isHidden = true;
                    self.btn_Shadow?.isHidden = true
                    self.blurView?.isHidden = true
                    self.dismiss(animated: false) {}
                })
            })
        })
        
    }
    
    // MARK: - @end
    // MARK: -
    
}
