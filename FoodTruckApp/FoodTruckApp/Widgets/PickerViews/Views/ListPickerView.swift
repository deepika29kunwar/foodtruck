//
//  MonthYearPicker.swift
//  MD
//
//  Created by Mahesh Dhakad on 02/12/17.
//  Copyright © 2017 MD. All rights reserved.
//

import UIKit


class ListPickerView: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    public var pickerViewList: [String]? = []

    var selectItem: String = "0" {
        didSet {
            selectRow((pickerViewList?.index(of: selectItem)!)!, inComponent: 0, animated: false)
        }
    }
    
    var onSelectedItem: ((_ item: String) -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonSetup()
    }
    

    func commonSetup() {
        // population List
       
        self.delegate = self
        self.dataSource = self
        
        if pickerViewList?.count != 0 {
            self.selectRow(0, inComponent: 0, animated: false)
        }
    }
    
    // Mark: UIPicker Delegate / Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerViewList?[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (pickerViewList?.count)!
    }
    

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let selectedItem = pickerViewList?[self.selectedRow(inComponent: 0)]
        
        if let block = onSelectedItem {
            block(selectedItem!)
        }
        
        self.selectItem = selectedItem!
        
    }
    
}

