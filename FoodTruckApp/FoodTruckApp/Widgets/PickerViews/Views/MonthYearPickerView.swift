//
//  MonthYearPicker.swift
//  MD
//
//  Created by Mahesh Dhakad on 02/12/17.
//  Copyright © 2017 MD. All rights reserved.
//

import UIKit

// DEFINE PICKER TYPE
enum PickerType:String{
    case MONTHS_YEARS
    case YEARS
    case OTHER
}

// IN CASE OF PICKER TYPE Month Year
enum PickerValue:Int {
    case LESS
    case GREATER
    case EQUAL
    case ANY
}

class MonthYearPickerView: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var months: [String]!
    var years: [Int]!
    var others: [String]!
    var pickerValue: Int = 0 // If Need Greater Month or Year do it 1

    public var pickerType = ""

    var month: Int = 0 {
        didSet {
            selectRow(month-1, inComponent: 0, animated: false)
        }
    }
    
    var year: Int = 0 {
        didSet {
            selectRow(years.index(of: year)!, inComponent: 1, animated: true)
        }
    }
    
    var other: String = "0" {
        didSet {
            selectRow(others.index(of: other)!, inComponent: 0, animated: false)
        }
    }
    
    var onSelectedMonthYear: ((_ month: Int, _ year: Int) -> Void)?
    var onSelectedYear: (( _ year: Int) -> Void)?
    var onSelectedOther: ((_ other: String) -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonSetup()
    }
    
    let currentMonth = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.month, from: NSDate() as Date)
    let currentYear = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.year, from: NSDate() as Date)

    func commonSetup() {
        // population years
        var years: [Int] = []
        if years.count == 0 {
            var year = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.year, from: NSDate() as Date)
            for _ in 1...15 {
                years.append(year)
                if pickerValue == PickerValue.LESS.rawValue
                {
                    year -= 1
                } else if pickerValue == PickerValue.GREATER.rawValue {
                    year += 1
                }
            }
        }
        
        if pickerType == PickerType.OTHER.rawValue {
            self.others = ["others"]
        } else {
            self.years = years
            self.selectRow(0, inComponent: 0, animated: false)
        }
        
        // population months with localized names
        var months: [String] = []
        var month = 0
        for _ in 1...12 {
            months.append(DateFormatter().monthSymbols[month].capitalized)
            month += 1
        }
        self.months = months

        self.delegate = self
        self.dataSource = self
        
        if pickerType == PickerType.MONTHS_YEARS.rawValue {
            self.selectRow(currentMonth - 1, inComponent: 0, animated: false)
        } else if pickerType == PickerType.YEARS.rawValue {
            self.selectRow(currentYear, inComponent: 0, animated: false)
        } else if pickerType == PickerType.OTHER.rawValue {
            self.selectRow(0, inComponent: 0, animated: false)
        }

        
        
    }
    
    // Mark: UIPicker Delegate / Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        if pickerType == PickerType.MONTHS_YEARS.rawValue {
            return 2
        } else  {
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerType == PickerType.MONTHS_YEARS.rawValue {
            
            switch component {
            case 0:
                return months[row]
            case 1:
                return "\(years[row])"
            default:
                return nil
            }
            
        } else if pickerType == PickerType.YEARS.rawValue {
           
            return "\(years[row])"
            
        } else {
            
            return "\(others[row])"
            
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerType == PickerType.MONTHS_YEARS.rawValue {
            switch component {
            case 0:
                return months.count
            case 1:
                return years.count
            default:
                return 0
            }
        } else if pickerType == PickerType.YEARS.rawValue {
            return years.count
        } else {
            return others.count
        }
        
    }
    

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerType == PickerType.MONTHS_YEARS.rawValue {
            
            let month = self.selectedRow(inComponent: 0)+1
            let year = years[self.selectedRow(inComponent: 1)]
            
            if pickerValue == PickerValue.GREATER.rawValue {
              
                // check months is not less then to current month
                if year == years[0] {
                    if month < currentMonth  {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.1, execute: {
                            self.selectRow(self.currentMonth - 1, inComponent: 0, animated: true)
                            if let block = self.onSelectedMonthYear {
                                block(self.currentMonth, year)
                            }
                        })
                    } else {
                        if let block = onSelectedMonthYear {
                            block(month, year)
                        }
                    }
                }else{
                    
                    if let block = onSelectedMonthYear {
                        block(month, year)
                    }
                }
                
            } else {
                
                if let block = onSelectedMonthYear {
                    block(month, year)
                }
            }
            
            self.month = month
            self.year = year
            
        } else if pickerType == PickerType.YEARS.rawValue {
            
            let year = years[self.selectedRow(inComponent: 0)]

            if let block = onSelectedYear {
                block(year)
            }
           // self.year = year

        } else {
            
            let other = others[self.selectedRow(inComponent: 0)]
            
            if let block = onSelectedOther {
                block(other)
            }
            self.other = other
        }
        
        
    }
    
}


