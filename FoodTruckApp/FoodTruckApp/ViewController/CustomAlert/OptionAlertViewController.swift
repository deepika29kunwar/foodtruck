//
//  OptionAlertViewController.swift
//  DEMOFOOD
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

enum optionListType: String {
    case view
    case edit
    case delete
    case cancel
}

protocol OptionAlertViewControllerDelegate {
    func removeOptionAlertViewControllerScr(option: optionListType)
}

class OptionAlertViewController: BaseAlertViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    var delegate : OptionAlertViewControllerDelegate?
    let optionArr = ["View","Edit","Delete","Cancel"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
      // tableHeight.constant = CGFloat((optionArr.count) * 45)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OptionTableViewCell
        cell.label_Option.text = optionArr[indexPath.row]
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true){
            if(self.delegate != nil){
                if indexPath.row == 0{
                    self.delegate?.removeOptionAlertViewControllerScr(option: optionListType.view)
                }else if indexPath.row == 1{
                    self.delegate?.removeOptionAlertViewControllerScr(option: optionListType.edit)
                }else if indexPath.row == 2{
                    self.delegate?.removeOptionAlertViewControllerScr(option: optionListType.delete)
                }else if indexPath.row == 3{
                    self.delegate?.removeOptionAlertViewControllerScr(option: optionListType.cancel)
                }
            }
        }
    }
}
