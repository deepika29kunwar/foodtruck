//
//  SimpleConfirmationAlertViewController.swift
//  Haulage
//
//  Created by consagous on 05/11/18.
//  Copyright © 2018 consagous. All rights reserved.
//

import UIKit

struct SimpleConfirmationAlertData {
    var title : String = ""
    init(title : String) {
        self.title  =   title
    }
}

class SimpleConfirmationAlertViewController: BaseAlertViewController {

    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var btnNo: ButtonLayerSetup!
    @IBOutlet weak var btnYes: ButtonLayerSetup!
    
    var simpleConfirmationAlertData : SimpleConfirmationAlertData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lblDescription.text    = self.simpleConfirmationAlertData != nil ? simpleConfirmationAlertData?.title : ""
        self.btnYes.setTitle("Ok", for: UIControl.State.normal)
        self.btnNo.setTitle("Cancel", for: UIControl.State.normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

}

//MARK:- Actions
//MARK:-
extension SimpleConfirmationAlertViewController {
    @IBAction func btnYesAction(_ sender : UIButton){
        self.dismiss(animated: true) {
            self.onClickActionOK(true)
        }
    }
    
    @IBAction func btnNoAction(_ sender : UIButton){
        self.dismiss(animated: true) {
            self.onClickActionOK(false)
        }
    }
}
//MARK:- Set Up Localized Strings
//MARK:-
extension SimpleConfirmationAlertViewController {
    
//    func setupLanguage(notification:Notification?) -> Void{
//
//        self.btnYes.setTitle(LocalizationKeys.yes.getLocalized(), for: UIControl.State.normal)
//        self.btnNo.setTitle(LocalizationKeys.no.getLocalized(), for: UIControl.State.normal)
//    }
    
}

//MARK:-
//MARK:- @end
