//
//  OptionTableViewCell.swift
//  DEMOFOOD
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class OptionTableViewCell: UITableViewCell {
    @IBOutlet weak var label_Option: UILabel!
    @IBOutlet weak var btn_Select: UIButton!
    @IBOutlet weak var background_View: UIView!
    @IBOutlet weak var image_Option: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
