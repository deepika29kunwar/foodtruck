//
//  HomeBaseViewController.swift
//  FoodTruckApp
//
//  Created by Apple on 25/09/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit
import Segmentio

class HomeBaseViewController: UIViewController {
    
    var segmentioView: Segmentio!
    var segmentiteam:Array<SegmentioItem> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let segmentioViewRect = CGRect(x: 0, y: 64, width: UIScreen.main.bounds.width, height: 64)
        segmentioView = Segmentio(frame: segmentioViewRect)
        view.addSubview(segmentioView)
        segmentioView.selectedSegmentioIndex=0
        // Do any additional setup after loading the view.
        let item1 = SegmentioItem(title: "" , image:#imageLiteral(resourceName: "userGray"))
        let item2 = SegmentioItem(title: "" , image:#imageLiteral(resourceName: "truckGray"))
        let item3 = SegmentioItem(title: "" , image:#imageLiteral(resourceName: "user"))
        segmentiteam.append(item1)
        segmentiteam.append(item2)
        segmentiteam.append(item3)
        let segmentState = SegmentioStates(
            defaultState: SegmentioState(
                backgroundColor: UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0),
                titleFont: UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
                titleTextColor: .black
            ),
            selectedState: SegmentioState(
                backgroundColor: UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0),
                titleFont: UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
                titleTextColor: .black
            ),
            highlightedState: SegmentioState(
                backgroundColor: UIColor.lightGray.withAlphaComponent(0.6),
                titleFont: UIFont.boldSystemFont(ofSize: UIFont.smallSystemFontSize),
                titleTextColor: .black
            )
        )
        
        segmentioView.setup(
            content: segmentiteam,
            style: SegmentioStyle.onlyImage,
            options: SegmentioOptions(backgroundColor: .clear, segmentPosition: .fixed(maxVisibleItems: 3), scrollEnabled: true, indicatorOptions: SegmentioIndicatorOptions(type: .bottom, ratio:1, height: 3, color: UIColor.getAppTHEME()), horizontalSeparatorOptions: nil, verticalSeparatorOptions: nil, imageContentMode: .center, labelTextAlignment: .center, labelTextNumberOfLines: 1, segmentStates: segmentState, animationDuration:0.3)
        )
        segmentioView.valueDidChange = { segmentio, segmentIndex in
            print("Selected item: ", segmentIndex)
            //self.delegate.didSelectedIndx(Idx: segmentIndex)
            // self.seletedTitle = self.subCategory.data.categoriesArray[segmentIndex].categoryName
            // self.subcatitems = self.subCategory.data.categoriesArray[segmentIndex]
            // self.collectionVW.reloadData()
            self.segmentAction(index: segmentIndex)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func segmentAction(index:Int)  {
        
    }

}
