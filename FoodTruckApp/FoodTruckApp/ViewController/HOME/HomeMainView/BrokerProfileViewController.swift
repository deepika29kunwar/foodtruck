//
//  BrokerProfileViewController
//  FoodTruckApp
//
//  Created by MacBook Pro on 19/08/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit

class BrokerProfileViewController: UIViewController, IndicatorInfoProvider {
  
    // MARK:- Outlet
    @IBOutlet weak var text_Name: UITextField!
    
     // MARK:- Var
    var itemInfo: IndicatorInfo = ""
    var termView : TermsView!
    var brokerProfile : BrokerProfileView!
    let optionArr = ["Location","Location Request"]
    let optionBackColorArr = [#colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1),#colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)]
    let optionBtnColorArr = [#colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1),#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)]
    let optionImage = [#imageLiteral(resourceName: "location"), #imageLiteral(resourceName: "locationRequest")]
    
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        termView = TermsView.instanceFromNib() as! TermsView
        termView.frame = self.view.bounds
        termView.delegate = self
        self.view.addSubview(termView)
        
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

//******** MARK:- Delegate calling of TermsView ********** //

extension BrokerProfileViewController: TermViewDelegate{
    func declineAction() {
        self.termView.viewDetail.isHidden = true
    }
    
    func acceptAction() {
        termView.isHidden = true
        
        brokerProfile = BrokerProfileView.instanceFromNib() as! BrokerProfileView
        brokerProfile.frame = self.view.bounds
        brokerProfile.delegate = self
        self.view.addSubview(brokerProfile)
        
    }
}


//******** MARK:- Delegate calling of Broker profile ********** //

extension BrokerProfileViewController: BrokerProfileDelegate{
    func createBrokerProfile() {
        brokerProfile.isHidden = true
    }
}

//******** MARK:- Tableview delegate and datasource method ********** //

extension BrokerProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OptionTableViewCell
        cell.label_Option.text = optionArr[indexPath.row]
        cell.background_View.backgroundColor =  optionBackColorArr[indexPath.row]
        cell.btn_Select.backgroundColor = optionBtnColorArr[indexPath.row]
        cell.image_Option.image = optionImage[indexPath.row]
        cell.btn_Select.tag = indexPath.row
        cell.btn_Select.addTarget(self, action: #selector(viewAllLocation(sender:)), for: .touchUpInside)
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
        
    }
    
    @objc func viewAllLocation(sender: UIButton){
        let story = UIStoryboard.init(name: "Home", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
