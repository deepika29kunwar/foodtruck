
//
//  UserFoodListViewController
//  FoodTruckApp
//
//  Created by MacBook Pro on 19/08/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit


class UserFoodListViewController: UIViewController, IndicatorInfoProvider {
    var itemInfo: IndicatorInfo = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

/**************************************************/
//MARK: - UITableViewDataSource,UITableViewDelegate
/**************************************************/
extension UserFoodListViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomTableViewCell
        
        if indexPath.row % 2 == 0{
            cell.contentView.backgroundColor = #colorLiteral(red: 0.9960784314, green: 0.8980392157, blue: 0.7058823529, alpha: 1)
        }else{
            cell.contentView.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9647058824, blue: 0.9019607843, alpha: 1)
        }
        
        return cell
    }
}

extension UserFoodListViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}
