//
//  HomeViewController.swift
//  FoodTruckApp
//
//  Created by MacBook Pro on 02/08/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit


class HomeViewController:ButtonBarPagerTabStripViewController  {
    
    @IBOutlet weak var barBUtton: ButtonBarView!
    
    
    @IBOutlet weak var containerViewUser: UIView!
    @IBOutlet weak var btnUser: UIButton!
    @IBOutlet weak var btnTruck: UIButton!
    @IBOutlet weak var containerViewTruck: UIView!
    @IBOutlet weak var btnBroker: UIButton!
    @IBOutlet weak var containerViewBroker: UIView!
    @IBOutlet weak var containerViewBrokerLocation: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // change selected bar color
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        settings.style.selectedBarBackgroundColor = #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
        settings.style.selectedBarHeight = 01
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 15
        settings.style.buttonBarRightContentInset = 15
        settings.style.buttonBarItemLeftRightMargin = 0
        
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.darkGray
            newCell?.label.textColor = UIColor.darkGray
        }
        
        buttonBarItemSpec = ButtonBarItemSpec.cellClass(width: { _ -> CGFloat in
            return 80  // <-- Your desired width
        })
        
        barBUtton.selectedBarHeight = 2.0
        barBUtton.selectedBar.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2549019608, alpha: 1)
        barBUtton.backgroundColor = UIColor.white
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let childOneVC = storyboard.instantiateViewController(withIdentifier: "UserFoodListViewController") as! UserFoodListViewController
        childOneVC.itemInfo.image = #imageLiteral(resourceName: "userGray")
        
        
        let childTwoVC = storyboard.instantiateViewController(withIdentifier: "OwnerProfileViewController") as! OwnerProfileViewController
        childTwoVC.itemInfo.image = #imageLiteral(resourceName: "truckGray")
        
        
        let childThreeVC = storyboard.instantiateViewController(withIdentifier: "BrokerProfileViewController") as! BrokerProfileViewController
        childThreeVC.itemInfo.image = #imageLiteral(resourceName: "user")

        return [childOneVC, childTwoVC, childThreeVC]
    }
    
    func setDefaultView(){
        self.containerViewTruck.isHidden = true
        self.containerViewBroker.isHidden = true
        self.containerViewUser.isHidden = false

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnMoreAction(sender:UIBarButtonItem)  {
        let buttonItemView = sender.value(forKey: "view") as? UIView
        
        let configuration = FTPopOverMenuConfiguration()
        configuration.backgroundColor = UIColor.white
        configuration.textColor = UIColor.black
        configuration.textFont = UIFont.systemFont(ofSize: 10)
        
        FTPopOverMenu.show(forSender: buttonItemView, withMenuArray: ["Help & Support","Logout"], imageArray: ["help","logout"], configuration: configuration, doneBlock: { (index) in
            print(index)
            if index == 0 {
                
            }else if index == 1{
                
            }else if index == 2 {
                
            }
        }) {
            print("dismiss")
        }
    }

    
    func actionBtnUser() {
        
        self.containerViewTruck.isHidden = true
        self.containerViewBroker.isHidden = true
        self.containerViewUser.isHidden = false

    }
    
    func actionBtnTruck() {
        
        self.containerViewUser.isHidden = true
        self.containerViewBroker.isHidden = true
        self.containerViewTruck.isHidden = false
        
    }
    
    func actionBtnBroker() {
        
        self.containerViewTruck.isHidden = true
        self.containerViewUser.isHidden = true
        self.containerViewBroker.isHidden = false
        
        //        self.btnTruck.setImage(#imageLiteral(resourceName: "truckGray"), for: .normal)
        //        self.btnBroker.setImage(#imageLiteral(resourceName: "userYellow"), for: .normal)
        //        self.btnUser.setImage(#imageLiteral(resourceName: "userGray"), for: .normal)
        //
        //        self.btnBroker.backgroundColor = #colorLiteral(red: 0.9999127984, green: 1, blue: 0.9998814464, alpha: 1)
        //        self.btnTruck.backgroundColor = #colorLiteral(red: 0.9685427547, green: 0.9686816335, blue: 0.9685124755, alpha: 1)
        //        self.btnUser.backgroundColor = #colorLiteral(red: 0.9685427547, green: 0.9686816335, blue: 0.9685124755, alpha: 1)
    }
    
    @IBAction func btnMenuAction(sender:UIButton) {
        self.findHamburguerViewController()?.showMenuViewController()
    }
}

extension HomeViewController{
    
    
}
