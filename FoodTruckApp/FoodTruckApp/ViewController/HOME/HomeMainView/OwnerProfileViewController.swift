//
//  OwnerProfileViewController
//  FoodTruckApp
//
//  Created by MacBook Pro on 19/08/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit
import SwiftyXMLParser


class OwnerProfileViewController: UIViewController, IndicatorInfoProvider {
    // MARK:- Outlet
    @IBOutlet weak var imageViewProfilePicture: UIImageView!
    @IBOutlet weak var text_Name: UITextField!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var ownerProfileView: UIView!
    
    
    // MARK:- Var
    var itemInfo: IndicatorInfo = ""
    var termView : TermsView!
    fileprivate var profileImageData:Data?
    fileprivate var actionSheetControllerIOS8: UIAlertController!
    fileprivate var isScreenDisplayFromImageController = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        termView = TermsView.instanceFromNib() as! TermsView
        termView.frame = self.view.bounds
        termView.delegate = self
        self.view.addSubview(termView)
        
        self.ownerProfileView.isHidden = true
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//******** MARK:- Delegate calling of TermsView ********** //

extension OwnerProfileViewController: TermViewDelegate{
    func declineAction() {
        self.termView.viewDetail.isHidden = true
    }
    
    func acceptAction() {
        termView.isHidden = true
        self.ownerProfileView.isHidden = false
    }
}

//**********MARK:- @IBAction ***********////
extension OwnerProfileViewController{
    @IBAction func didTappedOnLocationAll(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Owner", bundle:nil)
        let homeVC = storyBoard.instantiateViewController(withIdentifier: "AllLocationViewController") as! AllLocationViewController
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    @IBAction func didTappedOnCreateProfile(){
        
        
        //        if (imageViewProfilePicture.image == UIImage.init(named: "camera")){
        //            let message = "Please select profile"
        //            self.showAlert(title: "Error", message: message)
        //        }
        //        else
        if (text_Name.text?.isEmpty)! {
            let message = "Please enter name"
            self.showAlert(title: "Error", message: message)
        }
        else {
            self.serevrRequest()
        }
    }
    
    @IBAction func methodEditImage(_ sender: Any){
        self.displayPhotoSelectionOption()
    }
    
    fileprivate func displayPhotoSelectionOption(){
        actionSheetControllerIOS8 = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "cancel", style: .cancel) { action -> Void in
            AppConstant.kLogString("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            let option1ActionButton: UIAlertAction = UIAlertAction(title: "Camera", style: .default){ action -> Void in
                self.isScreenDisplayFromImageController = false
                AppConstant.kLogString("Camera")
                let cameraCapture:CameraCapture? = CameraCapture.sharedInstance() as? CameraCapture
                
                if(cameraCapture != nil){
                    cameraCapture!.launchCamera(on: self, withDelegate: self, withMediaTypeVideo: false, withMediaLibrary: false, withAllowBothTypeMedia: false, isImageCropRequire:true, withImageSize: CGSize.zero, withCircularAllow: true)
                }
            }
            
            actionSheetControllerIOS8.addAction(option1ActionButton)
        }
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)){
            let option2ActionButton: UIAlertAction = UIAlertAction(title: "Gallery", style: .default){ action -> Void in
                self.isScreenDisplayFromImageController = false
                
                AppConstant.kLogString("Gallery")
                let cameraCapture:CameraCapture? = CameraCapture.sharedInstance() as? CameraCapture
                
                if(cameraCapture != nil){
                    cameraCapture!.launchCamera(on: self, withDelegate: self, withMediaTypeVideo: false, withMediaLibrary: true, withAllowBothTypeMedia: false, isImageCropRequire:true, withImageSize: CGSize.zero, withCircularAllow: true)
                }
            }
            actionSheetControllerIOS8.addAction(option2ActionButton)
        }
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
}

//MARK:- CameraCaptureDelegate protocol method implementation

extension OwnerProfileViewController: CameraCaptureDelegate{
    
    func mediaCaptureInfo(withMediaName fileName: String!, withMediaType mediaType: String!, withMediaData mediaData: Data!) {
        if(mediaData != nil){
            let image = UIImage(data:mediaData,scale:1.0)
            self.imageViewProfilePicture.image = image
            self.profileImageData = mediaData
        }else{
            let alert = UIAlertController(title:nil, message:"unable to pick file", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title:"ok", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func cancelMediaCapture() {
        
    }
}


//******** MARK:- Delegate calling of Broker profile ********** //

extension OwnerProfileViewController: OwnerProfileDelegate{
    func createOwnerProfile() {
        self.ownerProfileView.isHidden = true
    }
}

extension OwnerProfileViewController:XMLParserDelegate{
    func serevrRequest()  {
        self.view.showHUD()
        APIClient.init().postRequest(withParams: ReqestParams.createProfile(name: text_Name.text!, mobile: "", email: "", password: ""), url: URLConstants.profile) { (data, response, error) in
            self.view.hideHUD()
            if data != nil {
                OperationQueue.main.addOperation() {
                    let datafoo = data as! Data
                    let str = String(data: datafoo, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                    print(str ?? "")
                    let asb = XMLParser(data: datafoo)
                    asb.delegate = self
                    asb.parse()
                    self.view.hideHUD()
                    
                    let xml = try! XML.parse(str!)
                    print(xml)
                    let element = xml["methodResponse"][ "params"]["param"]["value"]["int"].element?.text
                    print(element ?? "")
                    
                    self.termView.isHidden = true
                    self.ownerProfileView.isHidden = true
                    
                }
            }else{
                print(error.debugDescription)
            }
        }
    }
}
