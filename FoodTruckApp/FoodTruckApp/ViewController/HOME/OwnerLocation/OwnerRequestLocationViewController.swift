
//
//  CreateLocationViewController.swift
//  DemoUiDesign
//
//  Created by Apple on 12/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GooglePlaces
import GooglePlacePicker
import SwiftyXMLParser


class OwnerRequestLocationViewController: BaseViewController {
    @IBOutlet weak var txtSelectAddress: UITextField!
    @IBOutlet weak var txtStartTime: UITextField!
    @IBOutlet weak var txtEndTime: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    var defaultDate = Date()
    fileprivate var strLocation     =   ""
    fileprivate var strLatitude : String?
    fileprivate var strLongitude : String?
    var datePickerBool: Bool = true
    var strStartDate = String()
    var strEndDate = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

//**********MARK:- @IBAction ***********////
extension OwnerRequestLocationViewController{
    @IBAction func didTappedOnBack(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func methodStartDateAction(_ sender: UIButton) {
        print("Method Action....")
        self.openDatePickerWithTag(tag: 0)
    }
    
    @IBAction func methodEndDateAction(_ sender: UIButton) {
        print("Method Action....")
        self.openDatePickerWithTag(tag: 1)
    }
    
    @IBAction func methodSendRequestAction(_ sender: UIButton) {
        if isFormValid() {
            self.serevrRequest()
        }
    }
    //ToDo:- Open DatePicker
    func openDatePickerWithTag(tag : Int){
        
        let storyBoard = UIStoryboard.init(name: "PickerViews", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        if datePickerBool {
            vc.preSelectedDate = defaultDate
        }else{
            vc.preSelectedDate = Date()
        }
        vc.datePickerType = .Date
        if tag == 0 {
            vc.minimumDate =    Calendar.current.date(byAdding: .year, value: -60, to: Date())!
            vc.maximumDate =    Calendar.current.date(byAdding: .year, value: -20, to: Date())!
        }
        else{
            //  vc.maximumDate =    Calendar.current.date(byAdding: .month, value: 9, to: Date())!
            vc.minimumDate = Date()
            vc.maximumDate = Calendar.current.date(byAdding: .weekOfYear, value: 40, to: Date())!
        }
        
        vc.onSelectDone = { timeStamp, date in
            self.defaultDate = date
            let strWithDate = date.stringFromFormat("yyyy-MM-dd")
            
            
            if tag == 0 {
                self.strStartDate = strWithDate
                self.dismiss(animated: true, completion: nil)
                self.openTimePickerWithTag(tag: 0)
            }
            else{
                self.strEndDate = strWithDate
                self.dismiss(animated: true, completion: nil)
                self.openTimePickerWithTag(tag: 1)
            }
        }
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func openTimePickerWithTag(tag : Int){
        
        let storyBoard = UIStoryboard.init(name: "PickerViews", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        
        vc.datePickerType = .Time
        
        vc.minimumDate =    Calendar.current.date(byAdding: .year, value: -60, to: Date())!
        vc.maximumDate =    Calendar.current.date(byAdding: .year, value: -20, to: Date())!
        
        vc.onSelectDone = { timeStamp, date in
            self.defaultDate = date
            let strWithTime = date.stringFromFormat("hh:mm:ss")
            
            if tag == 0 {
                self.txtStartTime.text =  "\(self.strStartDate) \(strWithTime)"
            }
            else{
                self.txtEndTime.text =  "\(self.strEndDate) \(strWithTime)"
            }
        }
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Button_SpecificLocation(_ sender: Any) {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
    
    
    fileprivate func isFormValid() -> Bool{
        var isFound = true
        var message = ""
        
        if (txtSelectAddress.text?.isEmpty)! {
            message = "Please enter address"
            isFound = false
        } else if (txtStartTime.text?.isEmpty)! {
            message = "Please enter start time"
            isFound = false
        } else if (txtEndTime.text?.isEmpty)! {
            message = "Please enter end time"
            isFound = false
        } else if (self.txtPrice.text?.isEmpty)! {
            message = "Please enter price"
            isFound = false
        }
        
        if !isFound {
            //  self.showAlertViewControllerWith(title: "Error", message: message, buttonTitle: "Ok", controller: self)
        }
        
        return isFound
    }
    
    //MARK:- Show common alert view controller
    //    func showAlertViewControllerWith(title:String?, message:String, buttonTitle:String?, controller:UIViewController?){
    //        let banner = NotificationBanner(title: title!, subtitle: message, style: .success)
    //        banner.backgroundColor = color
    //        banner.titleLabel?.textColor = UIColor(hexString: ColorCode.whiteColor)
    //        banner.subtitleLabel?.textColor = UIColor(hexString: ColorCode.whiteColor)
    //        banner.show()
    //    }
}

//**********MARK:- GMSAutocompleteViewControllerDelegate ***********////
extension OwnerRequestLocationViewController:GMSAutocompleteViewControllerDelegate{
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        if(place.formattedAddress != nil){
            strLongitude = nil
            strLongitude = nil
            strLatitude = String(place.coordinate.latitude)
            strLongitude = String(place.coordinate.longitude)
            strLocation =   place.formattedAddress!
            //            ApplicationPreference.saveUsersAddress(address: place.formattedAddress!)
            //            ApplicationPreference.saveUserDefaultUserLat(userLatitude: String(place.coordinate.latitude), userLongitude: String(place.coordinate.longitude))
            self.txtSelectAddress.text = strLocation
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
    }
    
    // User cancelled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
}

//**********MARK:- XMLParserDelegate ***********////

extension OwnerRequestLocationViewController:XMLParserDelegate{
    func serevrRequest()  {
        self.view.showHUD()
        APIClient.init().postRequest(withParams: ReqestParams.createLocation(name: txtSelectAddress.text!, startTime: txtStartTime.text!, endTime: txtEndTime.text!, price: txtPrice.text!), url: URLConstants.profile) { (data, response, error) in
            self.view.hideHUD()
            if data != nil {
                OperationQueue.main.addOperation() {
                    let datafoo = data as! Data
                    let str = String(data: datafoo, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                    print(str ?? "")
                    let asb = XMLParser(data: datafoo)
                    asb.delegate = self
                    asb.parse()
                    
                    self.view.hideHUD()
                }
            }else{
                print(error.debugDescription)
            }
        }
    }
}
