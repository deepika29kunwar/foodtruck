//
//  BookLocationViewController.swift
//  DemoUiDesign
//
//  Created by Apple on 12/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class OwnerBookLocationListViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
//**********MARK:- @IBAction ***********////
extension OwnerBookLocationListViewController{
    @IBAction func didTappedOnBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTappedOnPlus(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Owner", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "OwnerRequestLocationViewController") as! OwnerRequestLocationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}



/**************************************************/
//MARK: - UITableViewDataSource,UITableViewDelegate
/**************************************************/
extension OwnerBookLocationListViewController : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BookLocationTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}


class BookLocationTableViewCell: UITableViewCell {
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
}


