//
//  RequestViewController.swift
//  DemoUiDesign
//
//  Created by Apple on 01/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class OwnerRequestViewController: BaseAlertViewController, IndicatorInfoProvider,OptionAlertViewControllerDelegate {
var itemInfo: IndicatorInfo = "Items"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    func removeOptionAlertViewControllerScr(option: optionListType) {
        switch option {
        case optionListType.view:
            print("view")
        case optionListType.edit:
            print("edit")
        case optionListType.delete:
            print("delete")
        case optionListType.cancel:
            print("cancel")
        default:
            print("default")
        }
    }
    
}


//**********MARK:- @IBAction ***********////
extension OwnerRequestViewController{
    @IBAction func didTappedOnPlus(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Owner", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "OwnerRequestLocationViewController") as! OwnerRequestLocationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


/**************************************************/
//MARK: - UITableViewDataSource,UITableViewDelegate
/**************************************************/
extension OwnerRequestViewController : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RequestTableViewCell
        
        cell.btnAccept.addTarget(self, action: #selector(self.actionBtnAccept), for: .touchUpInside)
            return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let optionAlertViewController : OptionAlertViewController = BaseApp.sharedInstance.getViewController(storyboardName: "CustomAlert", viewControllerName: OptionAlertViewController.nameOfClass)
       // simpleConfirmationAlertViewController.simpleConfirmationAlertData = SimpleConfirmationAlertData(title: title!)
        optionAlertViewController.onClickActionOK = { success in
            
            if success {
                self.navigationController?.popViewController(animated: true)
            } else {
                
            }
            
        }
        optionAlertViewController.presentAlertViewControllerOn(vc: self, and: CustomAlertAnimationType.transform)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
    }
    
    @objc func actionBtnAccept(){
//        self.openMyCommentsViewController()
    }
}


class RequestTableViewCell: UITableViewCell {
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
}


public extension NSObject{
    public class var nameOfClass: String{
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    public var nameOfClass: String{
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
    }
}
