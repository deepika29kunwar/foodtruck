//
//  AllLocationViewController.swift
//  DemoUiDesign
//
//  Created by Apple on 01/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AllLocationViewController: ButtonBarPagerTabStripViewController {
    
    @IBOutlet weak var barBUtton: ButtonBarView!
    @IBOutlet weak var containerViewLocation: UIView!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblRequest: UILabel!
    @IBOutlet weak var containerViewRequest: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // change selected bar color
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        settings.style.selectedBarBackgroundColor = #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
        settings.style.selectedBarHeight = 01
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 15
        settings.style.buttonBarRightContentInset = 15
        settings.style.buttonBarItemLeftRightMargin = 0
        
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.darkGray
            newCell?.label.textColor = UIColor.darkGray
        }
        
        buttonBarItemSpec = ButtonBarItemSpec.cellClass(width: { _ -> CGFloat in
            return 80  // <-- Your desired width
        })
        
        barBUtton.selectedBarHeight = 2.0
        barBUtton.selectedBar.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.6, blue: 0.2549019608, alpha: 1)
        barBUtton.backgroundColor = UIColor.white
    }
    // MARK: - PagerTabStripDataSource
     override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let storyboard = UIStoryboard(name: "Owner", bundle: nil)
        let childOneVC = storyboard.instantiateViewController(withIdentifier: "OwnerLocationViewController") as! OwnerLocationViewController
        childOneVC.itemInfo.title = "Location"
        //        childOneVC.onApplyFilter = {[weak self] value in
        //
        //            let str = value.stringToCommaSepratedDecimal()
        //            self?.lblWlletAmt?.attributedText = BaseApp.sharedInstance.setCurrentWithColor(currentStr: "\(AppConstant.appCurrency)\(String(describing: str))", str: AppConstant.appCurrency)
        //        }
        
        let childTwoVC = storyboard.instantiateViewController(withIdentifier: "OwnerRequestViewController") as! OwnerRequestViewController
        childTwoVC.itemInfo.title = "My Request"
        //        childTwoVC.onApplyFilter = {[weak self] value in
        //
        //            let str = value.stringToCommaSepratedDecimal()
        //
        //            self?.lblWlletAmt?.attributedText = BaseApp.sharedInstance.setCurrentWithColor(currentStr: "\(AppConstant.appCurrency)\(String(describing: str))", str: AppConstant.appCurrency)
        //        }
        
        
        return [childOneVC, childTwoVC]
    }

}

extension AllLocationViewController{
    @IBAction func didTappedOnBack(){
        self.navigationController?.popViewController(animated: true)
    }
}
