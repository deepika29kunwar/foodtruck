//
//  LocationViewController.swift
//  DemoUiDesign
//
//  Created by Apple on 01/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class OwnerLocationViewController: BaseViewController , IndicatorInfoProvider{
var itemInfo: IndicatorInfo = "Items"
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

//**********MARK:- @IBAction ***********////
extension OwnerLocationViewController{
    @IBAction func didTappedOnPlus(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Owner", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "OwnerBookLocationListViewController") as! OwnerBookLocationListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
