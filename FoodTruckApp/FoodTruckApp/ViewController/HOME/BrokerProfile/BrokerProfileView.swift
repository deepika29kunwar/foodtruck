//
//  BrokerProfileView.swift
//  FoodTruckApp
//
//  Created by Apple on 30/09/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit

protocol BrokerProfileDelegate {
    func createBrokerProfile()
}


class BrokerProfileView: UIView {
    
    var delegate : BrokerProfileDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK:- @IBAction
    @IBAction func createBrokerProfileBtn(_ sender: Any) {
        if delegate != nil {
            delegate?.createBrokerProfile()
        }
    }

}
