//
//  OwnerProfileView.swift
//  FoodTruckApp
//
//  Created by Apple on 30/09/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit

protocol OwnerProfileDelegate {
    func createOwnerProfile()
}

class OwnerProfileView: UIView {

    var delegate : OwnerProfileDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK:- @IBAction
    @IBAction func createOwnerProfileBtn(_ sender: Any) {
        if delegate != nil {
            delegate?.createOwnerProfile()
        }
    }

}
