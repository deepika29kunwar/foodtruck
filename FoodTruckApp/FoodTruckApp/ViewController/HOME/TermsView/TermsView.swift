//
//  TermsView.swift
//  FoodTruckApp
//
//  Created by Apple on 30/09/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit

protocol TermViewDelegate {
    func declineAction()
    func acceptAction()
}

class TermsView: UIView {
    
    // MARK:- Outlet
    @IBOutlet weak var btnDecline: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var viewDetail: ViewLayerSetup!
    
    // MARK:- Var
    var delegate : TermViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK:- @IBAction
    @IBAction func declineActionBtn(_ sender: Any) {
        if delegate != nil {
            delegate?.declineAction()
        }
    }
    @IBAction func acceptActionBtn(_ sender: Any) {
        if delegate != nil {
            delegate?.acceptAction()
        }
    }
    
}
