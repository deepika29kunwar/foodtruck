//
//  LocationViewController.swift
//  DEMOFOOD
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LocationViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource,OptionAlertViewControllerDelegate {
   
    @IBOutlet weak var table_View:UITableView!

    var locationArr = ["Vijay Nagar","Palasia","BhawarKua","RNT"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table_View.tableFooterView = UIView()

    }

    func removeOptionAlertViewControllerScr(option: optionListType) {
        switch option {
        case optionListType.view:
            print("view")
        case optionListType.edit:
            print("edit")
        case optionListType.delete:
            print("delete")
        case optionListType.cancel:
            print("cancel")
        default:
            print("default")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func didTappedOnAdd(_ sender: Any) {
        let story = UIStoryboard.init(name: "Home", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "CreateLocationViewController") as! CreateLocationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func actionBtnBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LocationList
        cell.lblLocationTitle.text = locationArr[indexPath.row]
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "OptionAlertViewController") as! OptionAlertViewController
     //   vc.providesPresentationContextTransitionStyle = true
     //   vc.modalTransitionStyle =   .crossDissolve
      //  vc.definesPresentationContext = true
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        }
}


class LocationList: UITableViewCell {
    
    @IBOutlet weak var lblLocationTitle:UILabel!
    @IBOutlet weak var lblLocationSubTitle:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
