//
//  ViewController.swift
//  DEMOFOOD
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class CreateLocationViewController: UIViewController {
    @IBOutlet weak var label_DropDown: UILabel!
    @IBOutlet weak var text_Country: UITextField!
//    let dropDown = DropDown()
//    let dropArr = ["title","demo","fb","label","test","uiui"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension CreateLocationViewController{
    @IBAction func actionBtnBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionBtnCreate(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    //    @IBAction func methodForDropDown(_ sender: UIButton){
    //
    //        dropDown.anchorView = label_DropDown
    //        dropDown.direction = .top
    //        dropDown.bottomOffset = CGPoint(x: 0, y: label_DropDown.bounds.height+100)
    //        dropDown.dataSource = dropArr
    //
    //        // Action triggered on selection
    //        dropDown.selectionAction = { [unowned self] (index:Int, item: String) in
    //            self.text_Country.text = item
    //        }
    //        dropDown.show()
    //    }
}
