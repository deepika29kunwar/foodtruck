//
//  BaseViewController.swift
//  FoodTruckApp
//
//  Created by MacBook Pro on 05/08/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit
import Segmentio

class BaseViewController: UIViewController {
     @IBOutlet weak var constraintNavigationHeight:NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.constraintNavigationHeight != nil){
            if AppConstant.DeviceType.IS_IPHONE_X{
                self.constraintNavigationHeight?.constant = AppConstant.IPHONE_X_DEFAULT_NAVIGATION_BAR_HEIGHT
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
