//
//  ForgotPasswordViewController.swift
//  FoodTruckApp
//
//  Created by Apple on 01/09/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseViewController {
    @IBOutlet weak var text_Email: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK: All Button Action
extension ForgotPasswordViewController{
    @IBAction func actionOfBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTappedOnForgotAction(sender:UIButton) {
        if (text_Email.text?.isEmpty)!{
            let message = "Please enter email"
            self.showAlert(title: "Error", message: message)
            
        }else if !(isValidEmail(testStr: text_Email.text!)){
            let message = "Please enter valid email"
            self.showAlert(title: "Error", message: message)
        }
        else {
            self.CallAPIforResetPassword(strEmail: text_Email.text!)
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

//MARK:- Server Api
extension ForgotPasswordViewController{
    
    func CallAPIforResetPassword(strEmail : String)  {
        self.view.showHUD()
        let params = ["login":strEmail]
        APIClient.init().postRequestAfterLogin(withParams: params, url: URLConstants.forgotpassword) { (data, response, error) in
            self.view.hideHUD()
            if let dict = data as? JSONDictionary {
                print(dict)
                if let str = dict["message"] as? String {
                    self.alert(title: "", message: str, tag: "Success")
                }else if let str = dict["error"] as? String{
                    self.alert(title: "", message: str, tag: "Error")
                }
            }else{
                self.showAlert(title: "", message: error.debugDescription)
            }
        }
    }
    
    func alert(title:String,message:String, tag: String)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "OK", style: .default) { (action) in
            if tag == "Success"{
                self.navigationController?.popViewController(animated: true)
            }
        }
        alert.addAction(actionOK)
        self.present(alert, animated: true, completion: nil)
    }
}
