
//
//  TermsViewController.swift
//  FoodTruckApp
//
//  Created by Apple on 01/09/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit

class TermsViewController: BaseViewController {
    
    @IBOutlet weak var webView:UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL (string: "https://bandesoft.com/term-and-condition")
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func didTappedOnBack(){
        self.navigateToBackScreen()
    }
}


