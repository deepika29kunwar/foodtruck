
//
//  LoginVC.swift
//  FoodTruckApp
//
//  Created by MacBook Pro on 01/08/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit
import SwiftyXMLParser


class LoginVC: BaseViewController, GIDSignInUIDelegate, GIDSignInDelegate{
    
    @IBOutlet weak var textField_Password: UITextField!
    @IBOutlet weak var textField_UserName: UITextField!
    @IBOutlet weak var view_UserName: UIView!
    @IBOutlet weak var view_Password: UIView!
    
    var strName:String!
    var strEmail:String!
    var strUserID:String!
    var socialToken:String!
    
    
    fileprivate var signUpVC:SignUpVC?
    fileprivate var forgotPasswordViewController:ForgotPasswordViewController?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        self.textFieldSetUp()
     
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//****************************
//MARK: TextFieldSetUp
//****************************
extension LoginVC{
    func textFieldSetUp(){
        textField_UserName.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        textField_Password.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        textField_UserName.text = "imadarsh.ap@gmail.com"
        textField_Password.text = "adarsh121!"
    }
}


//****************************
//MARK: All Button Action
//****************************
extension LoginVC{
    @IBAction func actionOfBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLoginAction(sender:UIButton) {
        if (textField_UserName.text?.isEmpty)!{
            let message = "Please enter username"
            self.showAlert(title: "Error", message: message)
            
        }
        else if (textField_Password.text?.isEmpty)! {
            let message = "Please enter password"
            self.showAlert(title: "Error", message: message)
        }
        else {
            self.loginApiCall()
        }
    }
    
    func setHomeDashboard(){
       BaseApp.appDelegate.rootViewController()
    }
    
    @IBAction func didTappedOnRegister(_ sender: Any) {
        signUpVC = nil
        signUpVC = BaseApp.sharedInstance.getViewController(storyboardName: AppConstant.onBoardstorybaord, viewControllerName: SignUpVC.nameOfClass)
        self.navigationController?.pushViewController(signUpVC!, animated: false)
    }
    
    @IBAction func didTappedOnForgotPass(){
        forgotPasswordViewController = nil
        forgotPasswordViewController = BaseApp.sharedInstance.getViewController(storyboardName: AppConstant.onBoardstorybaord, viewControllerName: ForgotPasswordViewController.nameOfClass)
        self.navigationController?.pushViewController(forgotPasswordViewController!, animated: false)
    }
}

//****************************
//MARK: UITextFieldDelegate
//****************************
extension LoginVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textField_UserName {
            view_UserName.layer.borderWidth = 1.0
            view_UserName.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        else if textField == textField_Password{
            view_Password.layer.borderWidth = 1.0
            view_Password.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textField_UserName {
            view_UserName.layer.borderWidth = 0
        }
        else if textField == textField_Password{
            view_Password.layer.borderWidth = 0
        }
    }
}

//****************************
//MARK: - Social Login
//****************************
extension LoginVC{
    @IBAction func Faceebook(_ sender: Any) {
        let login = FBSDKLoginManager()
        login.logOut()
        login.loginBehavior = FBSDKLoginBehavior.systemAccount
        login.logIn(withReadPermissions: ["public_profile", "email"], from: self, handler: {(result, error) in
            if error != nil {
                print("Error :  \(String(describing: error))")
            }
            else if (result?.isCancelled)! {
                
            }
            else {
                self.socialToken = FBSDKAccessToken.current().tokenString
                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name, picture.type(large), email, name, id, gender,birthday"]).start(completionHandler: {(connection, result, error) -> Void in
                    if error != nil{
                        print("Error : \(String(describing: error))")
                    }else{
                        
                        print("userInfo is \(String(describing: result)))")
                        
                        self.strUserID = (result as AnyObject).value(forKey: "id") as! String
                        self.strName = (result as AnyObject).value(forKey: "name") as! String
                        self.strEmail = (result as AnyObject).value(forKey: "email") as! String
                        
                        let dict = ["oauth_provider":"f","email":self.strEmail,"user_id":self.strUserID,"name":self.strName,"token":self.socialToken] as JSONDictionary
                        //self.socialApiCall(dict: dict)
                        self.setHomeDashboard()
                    }
                })
            }
            
        })
        
    }
    
    @IBAction func Google(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
        
    }
    
//****************************
//MARK: Google Login
//****************************
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if (error == nil) {
            self.strUserID = user.userID                  // For client-side use only!
            self.socialToken = user.authentication.idToken // Safe to send to the server
            self.strName = user.profile.name
            self.strEmail = user.profile.email
            let dict = ["oauth_provider":"g","email":strEmail,"user_id":strUserID,"name":strName,"token":socialToken] as JSONDictionary
            self.socialApiCall(dict: dict)
            if user.profile.hasImage{
                // crash here !!!!!!!! cannot get imageUrl here, why?
                //                let imageUrl = user.profile.imageURL(withDimension: 120)
                //                let imageUrl1 = signIn.currentUser.profile.imageURL(withDimension: UInt(0.1))
                //                let urlString: String = imageUrl!.absoluteString
                //                let imageData = urlString.data(using: String.Encoding.utf8)
                //                //Encoding
                //                self.strImage = (imageData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)))!
                //                print(self.strImage)
                //                //                strImage = (imageUrl?.absoluteString)!
                //                print(" image url: ", strImage)
            }
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
}

//****************************
//MARK:- Server Api
//****************************
extension LoginVC{
    func loginApiCall()  {
        self.view.showHUD()
        APIClient.init().postRequest(withParams: ReqestParams.login(email: textField_UserName.text!, password: textField_Password.text!), url: URLConstants.login) { (data, response, error) in
            self.view.hideHUD()
            if data != nil {
                if let datafoo = data as? Data {
                    let str = String(data: datafoo, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                    print(str ?? "")
                    OperationQueue.main.addOperation {
                        
                        let xml = try! XML.parse(str!)
                        print(xml)
                        let element = xml["methodResponse"][ "params"]["param"]["value"]["int"].element?.text
                        print(element ?? "")
                        // self.id = element
                        ApplicationPreference.saveUserId(userId: element!)
                        self.setHomeDashboard()
                    }
                }else{
                    if let dict = data as? JSONDictionary {
                        self.showAlert(title: "", message: dict["message"] as? String)
                    }
                }
            }else{
                print(error.debugDescription)
            }
        }
    }
    
    func socialApiCall(dict:JSONDictionary)  {
        self.view.showHUD()
        APIClient.init().postRequestAfterLogin(withParams: dict, url: URLConstants.googleLogin) { (data, response, error) in
            self.view.hideHUD()
            if let dict = data as? JSONDictionary {
                print(dict)
                self.setHomeDashboard()
            }else{
                self.showAlert(title: "", message: error.debugDescription)
                print(error ?? "")
            }
        }
    }
    
    func alert(title:String,message:String)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "OK", style: .default) { (action) in
            
        }
        alert.addAction(actionOK)
        self.present(alert, animated: true, completion: nil)
    }
}
