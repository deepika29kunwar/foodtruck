//
//  SignUpVC.swift
//  FoodTruckApp
//
//  Created by MacBook Pro on 01/08/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit
import SwiftyXMLParser

class SignUpVC: BaseViewController {
    
    @IBOutlet weak var text_UserName: UITextField!
    @IBOutlet weak var text_Email: UITextField!
    @IBOutlet weak var text_ConfirmPassword: UITextField!
    @IBOutlet weak var text_Password: UITextField!
    @IBOutlet weak var text_Mobile: UITextField!
    @IBOutlet weak var view_UserName: UIView!
    @IBOutlet weak var view_Password: UIView!
    @IBOutlet weak var view_Email: UIView!
    @IBOutlet weak var view_CPassword: UIView!
    @IBOutlet weak var view_Mobile: UIView!
    
    var id:String!
    
    fileprivate var termsViewController : TermsViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textFieldSetUp()
      
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//****************************
//MARK:- Textfield setup
//****************************
extension SignUpVC{
    func textFieldSetUp(){
        text_UserName.attributedPlaceholder = NSAttributedString(string: "Enter your name",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        text_Email.attributedPlaceholder = NSAttributedString(string: "Enter your email",
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        text_Mobile.attributedPlaceholder = NSAttributedString(string: "Enter your mobile",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        text_Password.attributedPlaceholder = NSAttributedString(string: "Enter password",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        text_ConfirmPassword.attributedPlaceholder = NSAttributedString(string: "Confirm password",
                                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
}


//****************************
//MARK: All Button Action
//****************************
extension SignUpVC{
    
    func setHomeDashboard(){
      BaseApp.appDelegate.rootViewController()
    }
    
    
    @IBAction func didTappedOnLogin(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTappedOnTermsCondition(){
        termsViewController = nil
        termsViewController = BaseApp.sharedInstance.getViewController(storyboardName: AppConstant.onBoardstorybaord, viewControllerName: TermsViewController.nameOfClass)
        self.navigationController?.pushViewController(termsViewController!, animated: false)
    }
    
    @IBAction func actionOfBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSignupAction(sender:UIButton) {
        if (text_UserName.text?.isEmpty)!{
            let message = "Please enter username"
            self.showAlert(title: "Error", message: message)
        }else if (text_Email.text?.isEmpty)! {
            let message = "Please enter email"
            self.showAlert(title: "Error", message: message)
        }else if !(isValidEmail(testStr: text_Email.text!)){
            let message = "Please enter valid email"
            self.showAlert(title: "Error", message: message)
        }else if (text_Mobile.text?.isEmpty)! {
            let message = "Please enter mobile"
            self.showAlert(title: "Error", message: message)
        }else if (text_Password.text?.isEmpty)! {
            let message = "Please enter password"
            self.showAlert(title: "Error", message: message)
        }else if (text_ConfirmPassword.text?.isEmpty)! {
            let message = "Please enter confirm password"
            self.showAlert(title: "Error", message: message)
        }else if(text_Password.text?.compare(text_ConfirmPassword.text!) != ComparisonResult.orderedSame){
            let message = "Password and confirm password should be same"
            self.showAlert(title: "Error", message: message)
        }
        else {
            loginApiCall()
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

//****************************
//MARK:- UITextFieldDelegate
//****************************
extension SignUpVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == text_UserName {
            view_UserName.layer.borderWidth = 1.0
            view_UserName.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        else if textField == text_Email{
            view_Email.layer.borderWidth = 1.0
            view_Email.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        else if textField == text_Password{
            view_Password.layer.borderWidth = 1.0
            view_Password.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        else if textField == text_ConfirmPassword{
            view_CPassword.layer.borderWidth = 1.0
            view_CPassword.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        else if textField == text_Mobile{
            view_Mobile.layer.borderWidth = 1.0
            view_Mobile.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == text_UserName {
            view_UserName.layer.borderWidth = 0.0
        }
        else if textField == text_Email{
            view_Email.layer.borderWidth = 0.0
        }
        else if textField == text_Password{
            view_Password.layer.borderWidth = 0.0
        }
        else if textField == text_ConfirmPassword{
            view_CPassword.layer.borderWidth = 0.0
        }
        else if textField == text_Mobile{
            view_Mobile.layer.borderWidth = 0.0
        }
    }
}


//****************************
//MARK:- XMLParserDelegate
//****************************
extension SignUpVC:XMLParserDelegate {
    // 1
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        print(elementName)
        print(attributeDict)
    }
    
    // 2
    private func parser(parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        print(elementName)
    }
    
    // 3
    private func parser(parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: .whitespacesAndNewlines)
        print(data)
    }
}

//****************************
//MARK:- Server Request
//****************************
extension SignUpVC{
    // ToDo:- For signup authentication
    func loginApiCall()  {
        self.view.showHUD()
        APIClient.init().postRequest(withParams: ReqestParams.loginAuth(), url: URLConstants.login) { (data, response, error) in
            self.view.hideHUD()
            if data != nil {
                OperationQueue.main.addOperation {
                    let datafoo = data as! Data
                    let str = String(data: datafoo, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                    print(str ?? "")
                    let asb = XMLParser(data: datafoo)
                    asb.delegate = self
                    asb.parse()
                    let xml = try! XML.parse(str!)
                    print(xml)
                    let element = xml["methodResponse"][ "params"]["param"]["value"]["int"].element?.text
                    print(element ?? "")
                    self.id = element
                    FoodTruckManager.share.commonid = element!
                    
                    self.signApiCall()
                }
            }else{
                print(error.debugDescription)
            }
        }
    }
    
    func signApiCall()  {
        self.view.showHUD()
        
        APIClient.init().postRequest(withParams: ReqestParams.signup(username: "", mobile: "", email: "", password: ""), url: URLConstants.signup) { (data, response, error) in
            self.view.hideHUD()
            if data != nil {
                
                OperationQueue.main.addOperation {
                    let datafoo = data as! Data
                    let str = String(data: datafoo, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                    print(str ?? "")
                    let asb = XMLParser(data: datafoo)
                    asb.delegate = self
                    asb.parse()
                    self.setHomeDashboard()
                    let xml = try! XML.parse(str!)
                    print(xml)
                    let element = xml["methodResponse"][ "params"]["param"]["value"]["int"].element?.text
                    print(element ?? "")
                    self.id = element
                    FoodTruckManager.share.commonid = element!
                }
               
            }else{
                print(error.debugDescription)
            }
        }
    }
    
}
