//
//  DLDemoMenuViewController.swift
//  DLHamburguerMenu
//
//  Created by Nacho on 5/3/15.
//  Copyright (c) 2015 Ignacio Nieto Carvajal. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgVWProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!

    
    // data
    let arrTitle = ["Help & Support","Logout"]
    
    // icon
    let arrIcons = ["help","logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        defaultConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         UIApplication.shared.statusBarView?.backgroundColor = UIColor.statusBarColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func defaultConfig()  {
        imgVWProfile.layer.cornerRadius = imgVWProfile.frame.size.height/2
        imgVWProfile.layer.masksToBounds = true
        tableView.register(UINib(nibName: MenuTableViewCell.stringRepresentation, bundle: nil), forCellReuseIdentifier: MenuTableViewCell.stringRepresentation)
    
    }
    
    @IBAction func btnCancelAcrion(sender:UIButton) {
        if let hamburguerViewController = self.findHamburguerViewController() {
            hamburguerViewController.hideMenuViewControllerWithCompletion {
                
            }
        }
    }
   
    
    // MARK: UITableViewDelegate&DataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MenuTableViewCell.stringRepresentation, for: indexPath) as! MenuTableViewCell
        cell.lblTitle.text = arrTitle[indexPath.row]
        cell.imgVWIcon.image = UIImage(named: arrIcons[indexPath.row])
        cell.contentView.backgroundColor = UIColor.clear
        //cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let nvc = self.mainNavigationController()
//        let viewController:UIViewController!
//
//        if indexPath.row == 0 {
//            if let vc = self.navigationController?.visibleViewController as? HomeViewController {
//                print(vc)
//                self.btnCancelAcrion(sender: UIButton())
//            }else{
//                viewController = self.storyboard?.instantiateViewController(withIdentifier: HomeViewController.stringRepresentation)
//                nvc.pushViewController(viewController, animated: true)
//                if let hamburguerViewController = self.findHamburguerViewController() {
//                    hamburguerViewController.hideMenuViewControllerWithCompletion({
//                        hamburguerViewController.contentViewController = nvc
//                    })
//                }
//            }
//        }else if indexPath.row == 1 {
//            if let vc = self.navigationController?.visibleViewController as? PreferencesViewController {
//               print(vc)
//                self.btnCancelAcrion(sender: UIButton())
//            }else{
//                viewController = PreferencesViewController.instantiateFromXIB() as PreferencesViewController
//                nvc.pushViewController(viewController, animated: true)
//                if let hamburguerViewController = self.findHamburguerViewController() {
//                    hamburguerViewController.hideMenuViewControllerWithCompletion({
//                        hamburguerViewController.contentViewController = nvc
//                    })
//                }
//            }
//        }else if indexPath.row == 2 {
//            if let vc = self.navigationController?.visibleViewController as? AboutUsViewController {
//                print(vc)
//                self.btnCancelAcrion(sender: UIButton())
//            }else{
//                viewController = self.storyboard?.instantiateViewController(withIdentifier: AboutUsViewController.stringRepresentation)
//               nvc.pushViewController(viewController, animated: true)
//                if let hamburguerViewController = self.findHamburguerViewController() {
//                    hamburguerViewController.hideMenuViewControllerWithCompletion({
//                        hamburguerViewController.contentViewController = nvc
//                    })
//                }
//            }
//        }else if indexPath.row == 3 {
//            if let vc = self.navigationController?.visibleViewController as? SubscriptionPlanViewController {
//                print(vc)
//                self.btnCancelAcrion(sender: UIButton())
//            }else{
//                viewController = self.storyboard?.instantiateViewController(withIdentifier: SubscriptionPlanViewController.stringRepresentation)
//                nvc.pushViewController(viewController, animated: true)
//                if let hamburguerViewController = self.findHamburguerViewController() {
//                    hamburguerViewController.hideMenuViewControllerWithCompletion({
//                        hamburguerViewController.contentViewController = nvc
//                    })
//                }
//            }
//        }else if indexPath.row == 4 {
//            if let vc = self.navigationController?.visibleViewController as? SettingViewController {
//                print(vc)
//                self.btnCancelAcrion(sender: UIButton())
//            }else{
//                viewController = self.storyboard?.instantiateViewController(withIdentifier: SettingViewController.stringRepresentation)
//               nvc.pushViewController(viewController, animated: true)
//                if let hamburguerViewController = self.findHamburguerViewController() {
//                    hamburguerViewController.hideMenuViewControllerWithCompletion({
//                        hamburguerViewController.contentViewController = nvc
//                    })
//                }
//            }
//        }else if indexPath.row == 5 {
//            if let vc = self.navigationController?.visibleViewController as? TermConditionViewController {
//                print(vc)
//                self.btnCancelAcrion(sender: UIButton())
//            }else{
//                viewController = self.storyboard?.instantiateViewController(withIdentifier: TermConditionViewController.stringRepresentation)
//                 nvc.pushViewController(viewController, animated: true)
//                if let hamburguerViewController = self.findHamburguerViewController() {
//                    hamburguerViewController.hideMenuViewControllerWithCompletion({
//                        hamburguerViewController.contentViewController = nvc
//                    })
//                }
//            }
//        }else if indexPath.row == 6 {
//            let vcs = self.navigationController?.viewControllers
//           // print(vcs)
//            for vc in (self.navigationController?.viewControllers)! {
//                if vc is LoginViewController {
//                    self.navigationController?.popToViewController(vc, animated: false)
//                }
//
//            }
//        }
        
    }
    
    
    // MARK: - Navigation
    
    func mainNavigationController() -> DLHamburguerNavigationController {
        return self.storyboard?.instantiateViewController(withIdentifier: "NavigationViewController") as! DLHamburguerNavigationController
    }
    
}
