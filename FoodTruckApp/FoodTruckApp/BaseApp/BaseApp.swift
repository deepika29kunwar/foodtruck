//
//  BaseApp.swift
//  FoodTruckApp
//
//  Created by Apple on 13/12/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import Foundation
import UIKit


class BaseApp : NSObject{
    //Variable declaration
    static let sharedInstance = BaseApp()
    internal static let appDelegate = UIApplication.shared.delegate as! AppDelegate

    
    // MARK:- Load viewController from storyboard (used in multi-storyboard)
    func getViewController<T>(storyboardName:String, viewControllerName:String) -> T {
        let storyBoard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: viewControllerName) as! T
        return viewController
    }
    
    
    //MARK:- Remove Optional from string
    func removeOptionalWordFromString(_ convertText:String) -> String{
        var convertedString = convertText
        convertedString = convertedString.replacingOccurrences(of: "Optional(", with: "")
        convertedString = convertedString.replacingOccurrences(of: ")", with: "")
        return convertedString
    }
    
}
